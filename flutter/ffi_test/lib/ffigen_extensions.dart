// Dart imports:
import 'dart:ffi';

// Package imports:
import 'package:ffi/ffi.dart';

extension StringExtensions on String {
  /// Creates a zero-terminated Utf8 code-unit array from this String.
  ///
  /// See [StringUtf8Pointer.toNativeUtf8] for details.
  ///
  /// Returns an [Pointer<Int8>]-allocated pointer to the result.
  Pointer<Int8> toInt8() {
    return toNativeUtf8().cast<Int8>();
  }
}

extension PointerExtensions<T extends NativeType> on Pointer<T> {
  /// Converts this Pointer<Int8> encoded string to a Dart string.
  ///
  /// See [Utf8Pointer.toDartString] for details.
  ///
  /// Decodes the UTF-8 code units of this zero-terminated byte array as
  /// Unicode code points and creates a Dart string containing those code
  /// points.
  ///
  /// If [length] is provided, zero-termination is ignored and the result can
  /// contain NUL characters.
  ///
  /// If [length] is not provided, the returned string is the string up til
  /// but not including  the first NUL character.
  String toDartString({int? length}) {
    if (T == Int8) {
      return cast<Utf8>().toDartString();
    }

    throw UnsupportedError('$T unsupported');
  }
}

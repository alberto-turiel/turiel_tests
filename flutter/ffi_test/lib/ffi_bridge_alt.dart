// ignore_for_file: non_constant_identifier_names

// Dart imports:
import 'dart:ffi' as ffi;

// Package imports:
import 'package:ffi/ffi.dart';

//import 'dart:io';
//bool _checkPath(String path) => File(path).existsSync();

typedef Lookup = ffi.Pointer<T> Function<T extends ffi.NativeType>(
    String symbolName);

class FFIBridge {
  /// Holds the symbol lookup function.
  final Lookup _lookup;

  /// The symbols are looked up in a [DynamicLibrary].
  /// The [path] must refer to a native library file which can be successfully loaded
  FFIBridge(String path) : _lookup = ffi.DynamicLibrary.open(path).lookup;

  /// The symbols are looked up in [dynamicLibrary].
  FFIBridge.fromDynamicLibrary(ffi.DynamicLibrary dynamicLibrary)
      : _lookup = dynamicLibrary.lookup;

  /// The symbols are looked up with [lookup].
  FFIBridge.fromLookup(Lookup lookup) : _lookup = lookup;

  /// unsigned addNative(unsigned a, unsigned b)
  int add(int a, int b) => _addNative(a, b);

  late final _addNativePtr =
      _lookup<ffi.NativeFunction<ffi.Uint32 Function(ffi.Uint32, ffi.Uint32)>>(
          'addNative');
  // also ffi.Int32 Function(ffi.Int32, ffi.Int32);
  late final _addNative = _addNativePtr.asFunction<int Function(int, int)>();

  /// double divNative(double a, double b)
  double div(double a, double b) => _divNative(a, b);

  late final _divNativePtr =
      _lookup<ffi.NativeFunction<ffi.Double Function(ffi.Double, ffi.Double)>>(
          'divNative');
  late final _divNative =
      _divNativePtr.asFunction<double Function(double, double)>();

  /// void fibonacci_init(
  ///  const unsigned long long a,
  ///  const unsigned long long b)
  void fibInit(int a, int b) => _fibonacci_init(a, b);

  late final _fibonacci_initPtr =
      _lookup<ffi.NativeFunction<ffi.Void Function(ffi.Uint64, ffi.Uint64)>>(
          'fibonacci_init');
  late final _fibonacci_init =
      _fibonacci_initPtr.asFunction<void Function(int, int)>();

  /// bool fibonacci_next()
  bool fibNext() => _fibonacci_next() != 0;

  late final _fibonacci_nextPtr =
      _lookup<ffi.NativeFunction<ffi.Uint8 Function()>>('fibonacci_next');
  late final _fibonacci_next = _fibonacci_nextPtr.asFunction<int Function()>();

  /// unsigned long long fibonacci_current()
  int fibCurrent() => _fibonacci_current();

  late final _fibonacci_currentPtr =
      _lookup<ffi.NativeFunction<ffi.Uint64 Function()>>('fibonacci_current');
  late final _fibonacci_current =
      _fibonacci_currentPtr.asFunction<int Function()>();

  /// unsigned fibonacci_index()
  int fibIndex() => _fibonacci_index();

  late final _fibonacci_indexPtr =
      _lookup<ffi.NativeFunction<ffi.Uint32 Function()>>('fibonacci_index');
  late final _fibonacci_index =
      _fibonacci_indexPtr.asFunction<int Function()>();

  /// char* str_get_random(int length)
  String getRandomString(int length) {
    var rndUtf8 = _str_get_random(length);
    var retValue = rndUtf8.toDartString(length: length);
    _free_string(rndUtf8);

    return retValue;
  }

  late final _str_get_randomPtr =
      _lookup<ffi.NativeFunction<ffi.Pointer<Utf8> Function(ffi.Int32)>>(
          'str_get_random');
  late final _str_get_random =
      _str_get_randomPtr.asFunction<ffi.Pointer<Utf8> Function(int)>();

  /// char* reverse(char* str, int length)
  String reverse(String s) {
    var sUtf8 = s.toNativeUtf8();
    var reversedMessageUtf8 = _reverse(sUtf8, s.length);
    var retValue = reversedMessageUtf8.toDartString();

    calloc.free(sUtf8);
    _free_string(reversedMessageUtf8);

    return retValue;
  }

  late final _reversePtr = _lookup<
      ffi.NativeFunction<
          ffi.Pointer<Utf8> Function(ffi.Pointer<Utf8>, ffi.Int32)>>('reverse');
  late final _reverse = _reversePtr
      .asFunction<ffi.Pointer<Utf8> Function(ffi.Pointer<Utf8>, int)>();

  // void free_string(char* str)
  late final _free_stringPtr =
      _lookup<ffi.NativeFunction<ffi.Void Function(ffi.Pointer<Utf8>)>>(
          'free_string');
  late final _free_string =
      _free_stringPtr.asFunction<void Function(ffi.Pointer<Utf8>)>();

  /// const char* hello_world()
  String helloWorld() => _hello_world().toDartString();

  late final _hello_worldPtr =
      _lookup<ffi.NativeFunction<ffi.Pointer<Utf8> Function()>>('hello_world');
  late final _hello_world =
      _hello_worldPtr.asFunction<ffi.Pointer<Utf8> Function()>();

  /// struct Coordinate create_coordinate(double latitude, double longitude)
  Coordinate createCoordinate(double latitude, double longitude) =>
      _createCoordinate(latitude, longitude);

  late final _createCoordinatePtr =
      _lookup<ffi.NativeFunction<CreateCoordinateNative>>('create_coordinate');
  late final _createCoordinate =
      _createCoordinatePtr.asFunction<CreateCoordinate>();

  /// Place create_place(char* name, double latitude, double longitude)
  Place createPlace(String name, double latitude, double longitude) =>
      _createPlace(name.toNativeUtf8(), latitude, longitude);

  late final _createPlacePtr =
      _lookup<ffi.NativeFunction<CreatePlaceNative>>('create_place');
  late final _createPlace = _createPlacePtr.asFunction<CreatePlace>();
}

// Example of handling a simple C struct
class Coordinate extends ffi.Struct {
  @ffi.Double()
  external double latitude;

  @ffi.Double()
  external double longitude;
}

// Example of a complex struct (contains a string and a nested struct)
class Place extends ffi.Struct {
  external ffi.Pointer<Utf8> name;

  external Coordinate coordinate;
}

// C function: struct Coordinate create_coordinate(double latitude, double longitude)
typedef CreateCoordinateNative = Coordinate Function(
    ffi.Double latitude, ffi.Double longitude);
typedef CreateCoordinate = Coordinate Function(
    double latitude, double longitude);

// C function: struct Place create_place(char *name, double latitude, double longitude)
typedef CreatePlaceNative = Place Function(
    ffi.Pointer<Utf8> name, ffi.Double latitude, ffi.Double longitude);
typedef CreatePlace = Place Function(
    ffi.Pointer<Utf8> name, double latitude, double longitude);

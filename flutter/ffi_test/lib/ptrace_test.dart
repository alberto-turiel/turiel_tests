// Dart imports:
import 'dart:ffi' as ffi;
import 'dart:io';

// Package imports:
import 'package:ffi/ffi.dart';

typedef SetStrFunction = ffi.Void Function(ffi.Pointer);
typedef SetStrFunctionDart = void Function(ffi.Pointer);
typedef GetStrFunction = ffi.Pointer Function();
typedef GetStrFunctionDart = ffi.Pointer Function();

bool _checkPath(String path) => File(path).existsSync();

class FFITrace {
  late SetStrFunction _set;
  late GetStrFunction _get;

  FFITrace() {
    String path = "F:/GIT/proakt/trunc/bin/vs2019/Release/PTRACE34.dll";
    //PGetProjectName

    // ignore: avoid_print
    print('${_checkPath(path)} $path');

    final dl = ffi.DynamicLibrary.open(path);

    //_set = dl
    //    .lookupFunction<SetStrFunction, SetStrFunctionDart>('PSetProjectName');
    _get = dl
        .lookupFunction<GetStrFunction, GetStrFunctionDart>('PGetProjectName');
  }

  setProjectName(String name) => _set(name.toNativeUtf16());
  getProjectName() => _get().toString();
}

//EXTERN_C void PTOOLS32API PSetProjectName( LPCSTR lpszProject )
//EXTERN_C LPCSTR PTOOLS32API PGetProjectName()

// Dart imports:
import 'dart:ffi' as ffi;
import 'dart:io';

// Package imports:
import 'package:ffi/ffi.dart';

typedef AddNative = ffi.Int32 Function(ffi.Int32, ffi.Int32);
typedef AddDart = int Function(int, int);

typedef DivNative = ffi.Double Function(ffi.Double, ffi.Double);
typedef DivDart = double Function(double, double);

typedef SetUint64x2Native = ffi.Void Function(ffi.Uint64, ffi.Uint64);
typedef SetIntx2Dart = void Function(int, int);

typedef GetUint8Native = ffi.Uint8 Function();
typedef GetUint64Native = ffi.Uint64 Function();
typedef GetIntDart = int Function();

typedef GetStrDart = ffi.Pointer<Utf8> Function();

typedef GetRandomStrNative = ffi.Pointer<Utf8> Function(ffi.Int32);
typedef GetRandomStrDart = ffi.Pointer<Utf8> Function(int);

// C function: char *reverse(char *str, int length)
typedef ReverseNative = ffi.Pointer<Utf8> Function(
    ffi.Pointer<Utf8> str, ffi.Int32 length);
typedef ReverseDart = ffi.Pointer<Utf8> Function(
    ffi.Pointer<Utf8> str, int length);

// C function: void free_string(char *str)
typedef FreeStringNative = ffi.Void Function(ffi.Pointer<Utf8> str);
typedef FreeStringDart = void Function(ffi.Pointer<Utf8> str);

bool _checkPath(String path) => File(path).existsSync();

class FFIBridge {
  late AddDart _add;
  late DivDart _div;
  late SetIntx2Dart _fibInit;
  late GetIntDart _fibNext;
  late GetIntDart _fibCurrent;
  late GetIntDart _fibIndex;
  late GetRandomStrDart _getRandomString;
  late GetStrDart _getString;
  late ReverseDart _reverse;
  late FreeStringDart _freeString;

  FFIBridge() {
    // ignore: unused_local_variable
    String _pathDebug =
        "F:/GIT/turiel_tests/flutter/ffi_test/windows_dll_sample/x64/Debug/windows_dll_sample.dll";
    // ignore: avoid_print
    print('${_checkPath(_pathDebug)} $_pathDebug');
    // ignore: unused_local_variable
    String _pathRelease =
        "F:/GIT/turiel_tests/flutter/ffi_test/windows_dll_sample/x64/Release/windows_dll_sample.dll";
    // ignore: avoid_print
    print('${_checkPath(_pathRelease)} $_pathRelease');

    String path = _pathRelease;

    final dl = ffi.DynamicLibrary.open(path);

    _add = dl.lookupFunction<AddNative, AddDart>('addNative');
    _div = dl.lookupFunction<DivNative, DivDart>('divNative');

    _fibInit =
        dl.lookupFunction<SetUint64x2Native, SetIntx2Dart>('fibonacci_init');
    _fibNext = dl.lookupFunction<GetUint8Native, GetIntDart>('fibonacci_next');
    _fibCurrent =
        dl.lookupFunction<GetUint64Native, GetIntDart>('fibonacci_current');
    _fibIndex =
        dl.lookupFunction<GetUint64Native, GetIntDart>('fibonacci_index');

    _getString = dl.lookupFunction<GetStrDart, GetStrDart>('hello_world');

    _reverse = dl.lookupFunction<ReverseNative, ReverseDart>('reverse');
    _freeString =
        dl.lookupFunction<FreeStringNative, FreeStringDart>('free_string');

    _getRandomString = dl
        .lookupFunction<GetRandomStrNative, GetRandomStrDart>('str_get_random');
  }

  int add(int a, int b) => _add(a, b);
  double div(double a, double b) => _div(a, b);

  void fibInit(int a, int b) => _fibInit(a, b);
  bool fibNext() => _fibNext() != 0;
  int fibCurrent() => _fibCurrent();
  int fibIndex() => _fibIndex();

  String helloWorld() => _getString().toDartString();
  String reverse(String s) {
    var sUtf8 = s.toNativeUtf8();
    var reversedMessageUtf8 = _reverse(sUtf8, s.length);
    var retValue = reversedMessageUtf8.toDartString();

    calloc.free(sUtf8);
    _freeString(reversedMessageUtf8);

    return retValue;
  }

  String getRandomString(int length) {
    var rndUtf8 = _getRandomString(length);
    var retValue = rndUtf8.toDartString();
    _freeString(rndUtf8);

    return retValue;
  }
}

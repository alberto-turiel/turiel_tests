// The Fibonacci recurrence relation describes a sequence F
// where F(n) is { n = 0, a
//               { n = 1, b
//               { n > 1, F(n-2) + F(n-1)
// for some initial integral values a and b.
// If the sequence is initialized F(0) = 1, F(1) = 1,
// then this relation produces the well-known Fibonacci
// sequence: 1, 1, 2, 3, 5, 8, 13, 21, 34, ...
//
// Initialize a Fibonacci relation sequence
// such that F(0) = a, F(1) = b.
// This function must be called before any other function.
void fibonacci_init(
    const unsigned long long a, const unsigned long long b);

// Produce the next value in the sequence.
// Returns true on success and updates current value and index;
// false on overflow, leaves current value and index unchanged.
int fibonacci_next();

// Get the current value in the sequence.
unsigned long long fibonacci_current();

// Get the position of the current value in the sequence.
unsigned fibonacci_index();

/** Adds 2 integers. */
unsigned addNative(unsigned a, unsigned b);

double divNative(double a, double b);
double sqrtNative(double x);

char* str_get_random(int len);
char* reverse(char* str, int length);
void free_string(char* str);

const char* hello_world();

/** comment Coordinate 
 * 2nd line
*/
struct Coordinate
{
    double latitude;
    double longitude;
};

// comment Place
// 2nd line
struct Place
{
    char* name;
    struct Coordinate coordinate;
};

struct Coordinate create_coordinate(double latitude, double longitude);
struct Place create_place(char* name, double latitude, double longitude);

/*
** CAPI3REF: Result Codes
** KEYWORDS: {result code definitions}
**
** Many SQLite functions return an integer result code from the set shown
** here in order to indicate success or failure.
**
** New error codes may be added in future versions of SQLite.
**
** See also: [extended result code definitions]
*/
#define SQLITE_OK           0   /* Successful result */
// beginning-of-error-codes
#define SQLITE_ERROR        1   /* Generic error */
#define SQLITE_INTERNAL     2   /* Internal logic error in SQLite */
#define SQLITE_PERM         3   /* Access permission denied */
#define SQLITE_IOERR       10   /* Some kind of disk I/O error occurred */

#define SQLITE_ERROR_MISSING_COLLSEQ   (SQLITE_ERROR | (1<<8))
#define SQLITE_ERROR_RETRY             (SQLITE_ERROR | (2<<8))
#define SQLITE_ERROR_SNAPSHOT          (SQLITE_ERROR | (3<<8))
#define SQLITE_IOERR_READ              (SQLITE_IOERR | (1<<8))
#define SQLITE_IOERR_SHORT_READ        (SQLITE_IOERR | (2<<8))


/* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_READONLY         0x00000001

/* Ok for sqlite3_open_v2() */
#define SQLITE_OPEN_READWRITE        0x00000002

// Ok for sqlite3_open_v2()
#define SQLITE_OPEN_CREATE           0x00000004

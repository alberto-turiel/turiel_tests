// SampleLibrary.h - Contains declarations of math functions
#pragma once
#include <string>
using namespace std;

#ifdef SAMPLE_LIBRARY_EXPORTS
#define SAMPLE_LIBRARY_API __declspec(dllexport)
#else
#define SAMPLE_LIBRARY_API __declspec(dllimport)
#endif

// The Fibonacci recurrence relation describes a sequence F
// where F(n) is { n = 0, a
//               { n = 1, b
//               { n > 1, F(n-2) + F(n-1)
// for some initial integral values a and b.
// If the sequence is initialized F(0) = 1, F(1) = 1,
// then this relation produces the well-known Fibonacci
// sequence: 1, 1, 2, 3, 5, 8, 13, 21, 34, ...
//
// Initialize a Fibonacci relation sequence
// such that F(0) = a, F(1) = b.
// This function must be called before any other function.
extern "C" SAMPLE_LIBRARY_API void fibonacci_init(
    const unsigned long long a, const unsigned long long b);

// Produce the next value in the sequence.
// Returns true on success and updates current value and index;
// false on overflow, leaves current value and index unchanged.
extern "C" SAMPLE_LIBRARY_API bool fibonacci_next();

// Get the current value in the sequence.
extern "C" SAMPLE_LIBRARY_API unsigned long long fibonacci_current();

// Get the position of the current value in the sequence.
extern "C" SAMPLE_LIBRARY_API unsigned fibonacci_index();

/** Adds 2 integers. */
extern "C" SAMPLE_LIBRARY_API unsigned addNative(unsigned a, unsigned b);

extern "C" SAMPLE_LIBRARY_API double divNative(double a, double b);
extern "C" SAMPLE_LIBRARY_API double sqrtNative(double x);

extern "C" SAMPLE_LIBRARY_API char* str_get_random(int len);
extern "C" SAMPLE_LIBRARY_API char* reverse(char* str, int length);
extern "C" SAMPLE_LIBRARY_API void free_string(char* str);
extern "C" SAMPLE_LIBRARY_API const char* hello_world();

struct Coordinate
{
    double latitude;
    double longitude;
};

struct Place
{
    char* name;
    struct Coordinate coordinate;
};

extern "C" SAMPLE_LIBRARY_API struct Coordinate create_coordinate(double latitude, double longitude);
extern "C" SAMPLE_LIBRARY_API struct Place create_place(char* name, double latitude, double longitude);
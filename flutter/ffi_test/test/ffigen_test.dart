// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// Dart imports:
import 'dart:ffi' as ffi;
import 'dart:io';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:ffi_test/ffigen_extensions.dart';
import 'package:ffi_test/ffigen_sample_library.dart';

void main() {
  //String path = "F:/GIT/turiel_tests/flutter/ffi_test/windows_dll_sample/x64/Debug/windows_dll_sample.dll";
  String path =
      "F:/GIT/turiel_tests/flutter/ffi_test/windows_dll_sample/x64/Release/windows_dll_sample.dll";

  final ffiBridge = SampleLibrary(ffi.DynamicLibrary.open(path));

  test('DLL path', () {
    expect(File(path).existsSync(), true);
    expect(ffiBridge, isNotNull);
  });

  test('FFIgen - math', () {
    var add = ffiBridge.addNative(1, 2);
    expect(add, 3);

    var resultDiv = ffiBridge.divNative(4.0, 2.0);
    expect(resultDiv, 2.0);

    var resultSqrt = ffiBridge.sqrtNative(4.0);
    expect(resultSqrt, 2.0);
  });

  test('FFIgen - fibonacci', () {
    var buf = StringBuffer();
    ffiBridge.fibonacci_init(0, 1);
    for (int i = 1; i < 94; i++) {
      buf.write(ffiBridge.fibonacci_current());
      buf.write(', ');
      if (ffiBridge.fibonacci_next() == 0) {
        break;
      }
    }
    expect(ffiBridge.fibonacci_index(), 93);
    // ignore: avoid_print
    print(buf.toString());
  });

  test('FFIgen - string', () {
    var hello = ffiBridge.hello_world().toDartString();
    expect(hello, "Hello World");

    var helloReverse =
        ffiBridge.reverse(hello.toInt8(), hello.length).toDartString();
    expect(helloReverse, "dlroW olleH");

    int length = 34;
    var rnd = ffiBridge.str_get_random(length).toDartString(length: length);
    expect(rnd.length, length);
  });

  test('FFIgen - struct', () {
    var coordinate = ffiBridge.create_coordinate(1.0, 2.0);
    var name = "name";
    var place = ffiBridge.create_place(name.toInt8(), 1.0, 2.0);
    expect(coordinate.latitude, place.coordinate.latitude);
    expect(coordinate.longitude, place.coordinate.longitude);
    var x = place.name.toDartString();
    expect(x, name);
  });
}

// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// Dart imports:
import 'dart:ffi' as ffi;
import 'dart:io';

// Package imports:
import 'package:ffi/ffi.dart';
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:ffi_test/ffi_bridge_alt.dart';

void main() {
  //String path = "F:/GIT/turiel_tests/flutter/ffi_test/windows_dll_sample/x64/Debug/windows_dll_sample.dll";
  String path =
      "F:/GIT/turiel_tests/flutter/ffi_test/windows_dll_sample/x64/Release/windows_dll_sample.dll";

  //final FFIBridge ffiBridge = FFIBridge(path);
  final FFIBridge ffiBridge =
      FFIBridge.fromDynamicLibrary(ffi.DynamicLibrary.open(path));

  test('DLL path', () {
    expect(File(path).existsSync(), true);
    expect(ffiBridge, isNotNull);
  });

  test('Manual - math', () {
    var resultAdd = ffiBridge.add(1, 2);
    expect(resultAdd, 3);

    var resultDiv = ffiBridge.div(4.0, 2.0);
    expect(resultDiv, 2.0);
  });

  test('Manual - fibonacci', () {
    var buf = StringBuffer();
    ffiBridge.fibInit(0, 1);
    for (int i = 1; i < 94; i++) {
      buf.write(ffiBridge.fibCurrent());
      buf.write(', ');
      if (!ffiBridge.fibNext()) {
        break;
      }
    }
    expect(ffiBridge.fibIndex(), 93);
    // ignore: avoid_print
    print(buf.toString());
  });

  test('Manual - string', () {
    var hello = ffiBridge.helloWorld();
    expect(hello, "Hello World");

    var helloReverse = ffiBridge.reverse(hello);
    expect(helloReverse, "dlroW olleH");

    int length = 34;
    var rnd = ffiBridge.getRandomString(length);
    expect(rnd.length, length);
  });

  test('Manual - struct', () {
    var coordinate = ffiBridge.createCoordinate(1.0, 2.0);
    var name = "name";
    var place = ffiBridge.createPlace(name, 1.0, 2.0);
    expect(coordinate.latitude, place.coordinate.latitude);
    expect(coordinate.longitude, place.coordinate.longitude);
    var x = place.name.toDartString();
    expect(x, name);
  });
}

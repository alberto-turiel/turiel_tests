// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// Dart imports:
import 'dart:ffi' as ffi;
import 'dart:io';

// Package imports:
import 'package:ffi/ffi.dart';
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:ffi_test/ptrace_test.dart';

void main() {
  test('PTRACE34', () {
    final FFITrace trace = FFITrace();

    var result = trace.getProjectName();

    // ignore: avoid_print
    print(result);
  });
}

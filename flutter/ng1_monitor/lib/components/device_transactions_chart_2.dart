// Flutter imports:
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ng1.dart';
import 'package:ngbt_components/ng1/label_amount.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:ngbt_log/ngbt_log.dart';

// Project imports:
import '/l10n/app_localizations.g.dart';

class BarChartSample2 extends StatefulWidget {
  const BarChartSample2({Key? key, required this.transactionData})
      : super(key: key);

  final List<dynamic> transactionData;

  @override
  State<StatefulWidget> createState() => BarChartSample2State();
}

class BarChartSample2State extends State<BarChartSample2> {
  //final Color leftBarColor = const Color(0xff53fdd7);
  //final Color rightBarColor = const Color(0xffff5182);
  final double width = 7;
  final double maxY = 20;

  late List<BarChartGroupData> rawBarGroups;
  late List<BarChartGroupData> showingBarGroups;

  int touchedGroupIndex = -1;

  late Map<CashTransactionType, _CashTransactionTypeStatistic> chartData;
  late List<CashTransactionType> orderedChartData;

  @override
  void initState() {
    super.initState();

    chartData = _createCharData(widget.transactionData);

    // most number of transactions first
    orderedChartData = List<CashTransactionType>.from(chartData.keys);
    orderedChartData
        .sort((a, b) => chartData[b]!.count.compareTo(chartData[a]!.count));

    List<BarChartGroupData> items = [];

    if (orderedChartData.isNotEmpty) {
      double maxAmount = chartData.values
          .reduce((curr, next) =>
              curr.amount!.doubleValue > next.amount!.doubleValue ? curr : next)
          .amount!
          .doubleValue;
      int maxTransactions = chartData.values
          .reduce((curr, next) => curr.count > next.count ? curr : next)
          .count;

      for (int i = 0; i < orderedChartData.length; i++) {
        var data = chartData[orderedChartData[i]];
        items.add(_makeGroupData(
            i,
            data!.count.toDouble() * maxY / maxTransactions,
            (data.amount != null
                    ? data.amount!.doubleValue < 0
                        ? data.amount!.doubleValue * -1
                        : data.amount!.doubleValue
                    : 0) *
                maxY /
                maxAmount));
      }
    }

    rawBarGroups = items;

    showingBarGroups = rawBarGroups;
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      //child: Card(
      //elevation: 10,
      //color: const Color(0xff2c4260),
      child: Padding(
        padding: const EdgeInsets.all(NgbtUiUtils.defaultPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            /*Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: const <Widget>[
                  SizedBox(width: 38),
                  Text(
                    'Transactions',
                    style: TextStyle(color: Colors.white, fontSize: 22),
                  ),
                  SizedBox(width: 4),
                  Text(
                    'state',
                    style: TextStyle(color: Color(0xff77839a), fontSize: 16),
                  ),
                ],
              ),*/
            //const SizedBox(height: 38),
            Expanded(
              child: BarChart(
                BarChartData(
                  maxY: maxY,
                  barTouchData: _getBarTouchData(context),
                  titlesData: _getFlTitlesData(context),
                  barGroups: showingBarGroups,
                ),
              ),
            ),
            NgbtUiUtils.verticalSeparator(),
          ],
        ),
        //    ),
      ),
    );
  }

  BarChartGroupData _makeGroupData(int x, double y1, double y2) {
    var ctt = orderedChartData[x];
    var negative = chartData[ctt] != null &&
        chartData[ctt]!.amount != null &&
        chartData[ctt]!.amount!.doubleValue < 0;

    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(
        y: y1,
        colors: [ctt.color],
        width: width,
      ),
      BarChartRodData(
        y: y2,
        colors: [negative ? Colors.red : Colors.green],
        width: width,
      ),
    ]);
  }

  BarTouchData _getBarTouchData(BuildContext context) => BarTouchData(
      touchTooltipData: BarTouchTooltipData(
        tooltipBgColor: Colors.grey,
        getTooltipItem: (_, group, __, ___) {
          var ctt = orderedChartData[group];
          var data = chartData[ctt];
          return data != null
              ? BarTooltipItem(
                  //AppLocalizations.of(context)!.byId(
                  //        'cash_transaction_type_value_${ctt.value}') +
                  //    "\n" +
                  AmountLabel.getText(data.amount,
                          Localizations.localeOf(context).toString()) +
                      "\n" +
                      AppLocalizations.of(context)!
                          .chars_device_transactions_total(data.count),
                  Theme.of(context).textTheme.bodyText1!)
              : null;
        },
      ),
      touchCallback: (flTouchEvent, response) {
        if (!flTouchEvent.isInterestedForInteractions) {
          setState(() {
            touchedGroupIndex = -1;
            showingBarGroups = List.of(rawBarGroups);
          });
          return;
        }

        touchedGroupIndex = response!.spot!.touchedBarGroupIndex;

        setState(() {
          if (flTouchEvent is PointerExitEvent ||
              flTouchEvent is PointerUpEvent) {
            touchedGroupIndex = -1;
            showingBarGroups = List.of(rawBarGroups);
          } else {
            showingBarGroups = List.of(rawBarGroups);
            if (touchedGroupIndex != -1) {
              var sum = 0.0;
              for (var rod in showingBarGroups[touchedGroupIndex].barRods) {
                sum += rod.y;
              }
              final avg =
                  sum / showingBarGroups[touchedGroupIndex].barRods.length;

              showingBarGroups[touchedGroupIndex] =
                  showingBarGroups[touchedGroupIndex].copyWith(
                barRods: showingBarGroups[touchedGroupIndex].barRods.map((rod) {
                  return rod.copyWith(y: avg);
                }).toList(),
              );
            }
          }
        });
      });

  FlTitlesData _getFlTitlesData(BuildContext context) => FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          rotateAngle: 90,
          reservedSize: 90,
          margin: 20,
          getTextStyles: (context, value) => TextStyle(
              color: orderedChartData[value.toInt()].color,
              fontWeight: FontWeight.bold,
              fontSize: 14),
          getTitles: (double value) {
            if (value.toInt() < orderedChartData.length) {
              return AppLocalizations.of(context)!.byId(
                  'cash_transaction_type_value_${orderedChartData[value.toInt()].name}');
            }
            return "?";
          },
        ),
        leftTitles: SideTitles(
          showTitles: false,
          getTextStyles: (context, value) =>
              Theme.of(context).textTheme.caption!,
          margin: 0,
          reservedSize: 0,
          getTitles: (value) {
            if (value == 0) {
              return '0';
            } else if (value == 10) {
              return '10';
            } else if (value == 19) {
              return '19';
            } else {
              return '';
            }
          },
        ),
      );

  Map<CashTransactionType, _CashTransactionTypeStatistic> _createCharData(
      List<dynamic> transactionData) {
    Map<CashTransactionType, _CashTransactionTypeStatistic> retValue = {};
    var keyAmount = 'device_amount';
    var keyType = 'transaction_type';

    for (var element in transactionData) {
      var transaction = Map.from(element);
      CashTransactionType cct = CashTransactionType.UNKNOWN;

      if (transaction.containsKey(keyType) && transaction[keyType] != null) {
        try {
          cct = CashTransactionType.values.byName(transaction[keyType]);
        } on ArgumentError catch (ex, stackTrace) {
          NgbtLogger.warning("DeviceTransactionsChart.createCharData",
              origin: this, error: ex, stackTrace: stackTrace);
        }
      }

      if (!retValue.containsKey(cct)) {
        retValue[cct] = _CashTransactionTypeStatistic();
      }
      retValue[cct]!.add(transaction[keyAmount]);
    }

    return retValue;
  }
}

class _CashTransactionTypeStatistic {
  int count;
  Amount? amount;

  _CashTransactionTypeStatistic() : count = 0;

  void add(dynamic value) {
    count++;
    if (value != null) {
      var newAmount = value is Amount ? value : Amount.fromKey(value);
      if (amount == null) {
        amount = newAmount;
      } else {
        amount = amount! + newAmount;
      }
    }
  }
}

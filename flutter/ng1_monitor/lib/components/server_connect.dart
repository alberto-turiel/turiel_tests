// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:provider/provider.dart';

// Project imports:
import '/l10n/app_localizations.g.dart';
import '/server_session.dart';

class ServerConnect extends StatefulWidget {
  const ServerConnect({Key? key}) : super(key: key);

  @override
  _ServerConnectState createState() => _ServerConnectState();
}

class _ServerConnectState extends State<ServerConnect> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _tecHost =
      TextEditingController(text: ng1_default_host);
  final TextEditingController _tecPort =
      TextEditingController(text: ng1_default_port.toString());
  final TextEditingController _tecUser = TextEditingController();
  final TextEditingController _tecPassword = TextEditingController();

  Future<void> showConnectDialog(
      BuildContext context, ServerSession session) async {
    return await showDialog(
      //barrierDismissible: false,
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              content: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        controller: _tecHost,
                        validator: (value) {
                          return value != null && value.isNotEmpty
                              ? null
                              : AppLocalizations.of(context)!
                                  .server_connect_ip_error;
                        },
                        decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)!
                                .server_connect_ip),
                      ),
                      TextFormField(
                        controller: _tecPort,
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          return value != null &&
                                  value.isNotEmpty &&
                                  int.tryParse(value) != null
                              ? null
                              : AppLocalizations.of(context)!
                                  .server_connect_port_error;
                        },
                        decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)!
                                .server_connect_port),
                      ),
                      TextFormField(
                        controller: _tecUser,
                        validator: (value) {
                          return value != null && value.isNotEmpty
                              ? null
                              : AppLocalizations.of(context)!
                                  .server_connect_user_error;
                        },
                        decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)!
                                .server_connect_user),
                      ),
                      TextFormField(
                        controller: _tecPassword,
                        obscureText: true,
                        validator: (value) {
                          return value != null && value.isNotEmpty
                              ? null
                              : AppLocalizations.of(context)!
                                  .server_connect_password_error;
                        },
                        decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)!
                                .server_connect_password),
                      ),
                    ],
                  )),
              title: Text(AppLocalizations.of(context)!.server_connect_connect),
              actions: <Widget>[
                NgbtElevatedButton(
                  type: NgbtButtonType.ok,
                  onPressed: () async {
                    if (_formKey.currentState != null &&
                        _formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      // Do something like updating SharedPreferences or User Settings etc.
                      int? port = int.tryParse(_tecPort.value.text);
                      if (port != null) {
                        Exception? e = await session.connectToServer(
                            _tecHost.value.text,
                            port,
                            _tecUser.value.text,
                            _tecPassword.value.text);
                        if (e == null) {
                          Navigator.of(context).pop();
                        } else {
                          String message;

                          if (e is APIException) {
                            String messageId = e.code == ErrorCode.Unknown
                                ? e.message
                                : e.code.toString();

                            message = AppLocalizations.of(context)!
                                .byId('server_connect_error_$messageId');
                          } else {
                            message = e.toString();
                          }

                          showAlertDialog(context, message);
                        }
                      }
                    }
                  },
                ),
                const NgbtElevatedButton(type: NgbtButtonType.cancel),
              ],
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _ServerConnectButton(openDialog: (ServerSession session) async {
      await showConnectDialog(context, session);
    });
  }
}

class _ServerConnectButton extends StatelessWidget {
  final Function openDialog;

  const _ServerConnectButton({Key? key, required this.openDialog})
      : super(key: key);

  Widget getWidget(ServerSession session, BuildContext context) {
    switch (session.status) {
      case ServerSessionStatus.notConnected:
        return FloatingActionButton(
          onPressed: () => openDialog(session),
          tooltip: AppLocalizations.of(context)!.server_connect_connect + "...",
          child: const Icon(Icons.link),
        );
      case ServerSessionStatus.connecting:
        return FloatingActionButton(
          onPressed: null,
          tooltip: AppLocalizations.of(context)!.server_connect_connecting,
          child: const Icon(Icons.blur_on),
        );
      case ServerSessionStatus.connected:
      case ServerSessionStatus.disconnecting:
        return const SizedBox.shrink();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ServerSession>(
        builder: (context, session, child) => getWidget(session, context));
  }
}

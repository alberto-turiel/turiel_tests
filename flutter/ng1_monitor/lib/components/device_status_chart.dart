// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/charts/ngbt_chart_indicator.dart';
import 'package:ngbt_components/charts/ngbt_piechart.dart';
import 'package:ngbt_components/ng1.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:provider/provider.dart';

// Project imports:
import '/l10n/app_localizations.g.dart';
import '/server_session.dart';

class DeviceStatusChart extends StatelessWidget {
  const DeviceStatusChart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ServerSession>(
        builder: (context, session, child) => getWidget(session, context));
  }

  Widget getWidget(ServerSession session, BuildContext context) {
    var deviceList = session.deviceList;
    var chartData = _createCharData(deviceList);

    // sort by priority
    var orderedChartData = List<DeviceStatusValue>.from(chartData.keys);
    orderedChartData.sort((a, b) => b.severity.compareTo(a.severity));

    return NgbtPieChart(
      title: AppLocalizations.of(context)!.chars_device_status_title,
      subtitle: AppLocalizations.of(context)!.all_network,
      chartNumber: orderedChartData.length,
      emptyText: AppLocalizations.of(context)!.chars_no_data,
      legendBuilder: (int touchedIndex) =>
          _legendBuilder(touchedIndex, context, chartData, orderedChartData),
      pieSectionsBuilder: (int touchedIndex) => _pieSectionsBuilder(
          touchedIndex,
          context,
          chartData,
          orderedChartData,
          deviceList.length),
    );
  }
}

Map<DeviceStatusValue, int> _createCharData(List<Device> deviceList) {
  Map<DeviceStatusValue, int> retValue = {};
  deviceList
      .map<DeviceStatusValue>(
          (dev) => dev.status?.name ?? DeviceStatusValue.UNKNOWN)
      .fold<Map<DeviceStatusValue, int>>(retValue, (prev, item) {
    prev[item] = prev.containsKey(item) ? (prev[item]! + 1) : 1;
    return prev;
  });

  return retValue;
}

List<NgbtChartIndicator> _legendBuilder(
  int touchedIndex,
  BuildContext context,
  Map<DeviceStatusValue, int> chartData,
  List<DeviceStatusValue> orderedChartData,
) {
  return orderedChartData
      .map((e) => NgbtChartIndicator(
            color: e.color,
            isSquare: false,
            selected:
                touchedIndex >= 0 ? orderedChartData[touchedIndex] == e : false,
            text: AppLocalizations.of(context)!
                .byId('device_status_value_${e.name}'),
          ))
      .toList();
}

List<PieChartSectionData> _pieSectionsBuilder(
  int touchedIndex,
  BuildContext context,
  Map<DeviceStatusValue, int> chartData,
  List<DeviceStatusValue> orderedChartData,
  int totalDevices,
) {
  List<PieChartSectionData> pieCharts = [];

  for (var key in orderedChartData) {
    var selected = touchedIndex == pieCharts.length;
    pieCharts.add(PieChartSectionData(
      radius: selected ? 110 : 100,
      color: key.color,
      titleStyle: TextStyle(
          color: NgbtUiUtils.getContrastColor(key.color),
          fontSize: selected ? 20 : 16,
          height: 1,
          fontWeight: FontWeight.bold,
          backgroundColor: selected ? key.color : Colors.transparent),
      value: chartData[key]!.toDouble(),
      title: selected
          ? AppLocalizations.of(context)!
                  .chars_device_status_devices(chartData[key]!) +
              "\n" +
              ((chartData[key]! / totalDevices) * 100).toStringAsFixed(1) +
              "%"
          : chartData[key]!.toString(),
    ));
  }
  return pieCharts;
}

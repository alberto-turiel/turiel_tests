// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/charts/ngbt_chart_indicator.dart';
import 'package:ngbt_components/charts/ngbt_piechart.dart';
import 'package:ngbt_components/ng1.dart';
import 'package:ngbt_components/ng1/label_amount.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:ngbt_log/ngbt_log.dart';

// Project imports:
import '/l10n/app_localizations.g.dart';

class DeviceTransactionsChart extends StatelessWidget {
  const DeviceTransactionsChart(
      {Key? key, this.title, this.subtitle, required this.transactionData})
      : super(key: key);

  final String? title;
  final String? subtitle;
  final List<dynamic> transactionData;

  @override
  Widget build(BuildContext context) {
    var chartData = _createCharData(transactionData);

    // most number of transactions first
    var orderedChartData = List<CashTransactionType>.from(chartData.keys);
    orderedChartData
        .sort((a, b) => chartData[b]!.count.compareTo(chartData[a]!.count));

    return NgbtPieChart(
      title: title,
      subtitle: subtitle,
      emptyText: AppLocalizations.of(context)!.chars_no_data,
      chartNumber: orderedChartData.length,
      legendBuilder: (int touchedIndex) =>
          _legendBuilder(touchedIndex, context, chartData, orderedChartData),
      pieSectionsBuilder: (int touchedIndex) => _pieSectionsBuilder(
          touchedIndex,
          context,
          chartData,
          orderedChartData,
          transactionData.length),
    );
  }

  Map<CashTransactionType, _CashTransactionTypeStatistic> _createCharData(
      List<dynamic> transactionData) {
    Map<CashTransactionType, _CashTransactionTypeStatistic> retValue = {};
    var keyAmount = 'device_amount';
    var keyType = 'transaction_type';

    for (var element in transactionData) {
      var transaction = Map.from(element);
      CashTransactionType cct = CashTransactionType.UNKNOWN;

      if (transaction.containsKey(keyType) && transaction[keyType] != null) {
        try {
          cct = CashTransactionType.values.byName(transaction[keyType]);
        } on ArgumentError catch (ex, stackTrace) {
          NgbtLogger.warning("DeviceTransactionsChart.createCharData",
              origin: this, error: ex, stackTrace: stackTrace);
        }
      }

      if (!retValue.containsKey(cct)) {
        retValue[cct] = _CashTransactionTypeStatistic();
      }
      retValue[cct]!.add(transaction[keyAmount]);
    }

    return retValue;
  }

  List<NgbtChartIndicator> _legendBuilder(
    int touchedIndex,
    BuildContext context,
    Map<CashTransactionType, _CashTransactionTypeStatistic> chartData,
    List<CashTransactionType> orderedChartData,
  ) {
    return orderedChartData
        .map((e) => NgbtChartIndicator(
              color: e.color,
              isSquare: false,
              selected: touchedIndex >= 0
                  ? orderedChartData[touchedIndex] == e
                  : false,
              text: AppLocalizations.of(context)!
                  .byId('cash_transaction_type_value_${e.name}'),
            ))
        .toList();
  }

  List<PieChartSectionData> _pieSectionsBuilder(
    int touchedIndex,
    BuildContext context,
    Map<CashTransactionType, _CashTransactionTypeStatistic> chartData,
    List<CashTransactionType> orderedChartData,
    int total,
  ) {
    var locale = Localizations.localeOf(context).toString();
    List<PieChartSectionData> pieCharts = [];

    for (var key in orderedChartData) {
      if (chartData[key]!.count > 0) {
        var selected = touchedIndex == pieCharts.length;
        pieCharts.add(PieChartSectionData(
          radius: selected ? 110 : 100,
          color: key.color,
          titleStyle: TextStyle(
              color: NgbtUiUtils.getContrastColor(key.color),
              fontSize: selected ? 20 : 16,
              height: 1,
              fontWeight: FontWeight.bold,
              backgroundColor: selected ? key.color : Colors.transparent),
          value: chartData[key]!.count.toDouble(),
          title: selected
              ? AppLocalizations.of(context)!
                      .byId('cash_transaction_type_value_${key.name}') +
                  "\n" +
                  AppLocalizations.of(context)!
                      .chars_device_transactions_total(chartData[key]!.count) +
                  "\n" +
                  AmountLabel.getText(chartData[key]!.amount, locale)
              : chartData[key]!.count.toString(),
        ));
      }
    }
    return pieCharts;
  }
}

class _CashTransactionTypeStatistic {
  int count;
  Amount? amount;

  _CashTransactionTypeStatistic() : count = 0;

  void add(dynamic value) {
    count++;
    if (value != null) {
      var newAmount = value is Amount ? value : Amount.fromKey(value);
      if (amount == null) {
        amount = newAmount;
      } else {
        amount = amount! + newAmount;
      }
    }
  }
}

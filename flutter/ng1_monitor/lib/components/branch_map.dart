// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong2/latlong.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ng1.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:provider/provider.dart';

// Project imports:
import '/server_session.dart';

const markersSide = 0.1;

class BranchMap extends StatefulWidget {
  const BranchMap({
    Key? key,
    this.visibleDevices,
  }) : super(key: key);

  final List<int>? visibleDevices;

  @override
  BranchMapState createState() => BranchMapState();
}

class BranchMapState extends State<BranchMap> {
  late MapController mapController;
  final _markersNotifier = ValueNotifier<List<Marker>>([]);

  @override
  void initState() {
    super.initState();
    mapController = MapController();
    mapController.onReady.then((value) {
      mapController.fitBounds(_getCenterMapForMarkers());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ServerSession>(
        builder: (context, session, child) => getWidget(session, context));
  }

  Widget getWidget(ServerSession session, BuildContext context) {
    _markersNotifier.value = _getMarkers(session);

    return ValueListenableBuilder(
      valueListenable: _markersNotifier,
      builder: (BuildContext context, List<Marker> markers, Widget? child) {
        if (markers.isNotEmpty) {
          mapController.fitBounds(_getCenterMapForMarkers(markers));
        }

        return Container(
          padding: const EdgeInsets.all(NgbtUiUtils.defaultPadding),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppLocalizations.of(context)!.branch_map_title,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  _getMapControls(markers),
                ],
              ),
              NgbtUiUtils.verticalSeparator(),
              Flexible(
                child: FlutterMap(
                  mapController: mapController,
                  options: MapOptions(
                      /*onPositionChanged: (position) {
                        zoom = position.zoom;
                      },*/
                      ),
                  layers: [
                    TileLayerOptions(
                        urlTemplate:
                            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                        subdomains: ['a', 'b', 'c']),
                    MarkerLayerOptions(
                      markers: markers,
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  List<Marker> _getMarkers(ServerSession serverSession) {
    Map<int, Marker> branchMarkerMap = <int, Marker>{};
    Map<int, int> branchSeverity = <int, int>{};

    if (serverSession.status == ServerSessionStatus.connected) {
      var deviceList =
          widget.visibleDevices == null || widget.visibleDevices!.isEmpty
              ? serverSession.deviceList
              : serverSession.deviceList
                  .where((d) => widget.visibleDevices!.contains(d.id))
                  .toList();

      for (Device device in deviceList) {
        Branch? branch = device.branch;
        if (branch != null) {
          var devStatus = device.status?.name ?? DeviceStatusValue.UNKNOWN;

          if (branch.id != null &&
              device.branchLatLong != null &&
              (!branchSeverity.containsKey(branch.id) ||
                  branchSeverity[branch.id]! < devStatus.severity)) {
            // update severity
            branchSeverity[branch.id!] = devStatus.severity;
            Marker marker = Marker(
                width: 80.0,
                height: 80.0,
                //point: _branchCoords[branch.id!]!,
                point: device.branchLatLong!,
                builder: (ctx) => Icon(
                      Icons.location_on,
                      color: devStatus.color,
                      size: 30,
                    ));
            branchMarkerMap[device.branchId!] = marker;
          }
        }
      }
    }
    return branchMarkerMap.values.toList();
  }

  LatLngBounds _getCenterMapForMarkers([List<Marker>? markers]) {
    LatLng? sw;
    LatLng? ne;

    if (markers != null) {
      for (Marker marker in markers) {
        if (sw == null || ne == null) {
          sw = LatLng(marker.point.latitude, marker.point.longitude);
          ne = LatLng(marker.point.latitude, marker.point.longitude);
        } else {
          if (marker.point.latitude > sw.latitude) {
            sw.latitude = marker.point.latitude;
          }
          if (marker.point.latitude < ne.latitude) {
            ne.latitude = marker.point.latitude;
          }
          if (marker.point.longitude < sw.longitude) {
            sw.longitude = marker.point.longitude;
          }
          if (marker.point.longitude > ne.longitude) {
            ne.longitude = marker.point.longitude;
          }
        }
      }
    }

    /// by default NGBT LatLong
    sw ??= LatLng(48.284200, 11.563090);
    sw.latitude -= markersSide;
    sw.longitude -= markersSide;

    ne ??= LatLng(48.284200, 11.563090);
    ne.latitude += markersSide;
    ne.longitude += markersSide;

    return LatLngBounds(sw, ne);
  }

  Widget _getMapControls(List<Marker> markers) {
    return Row(children: [
      IconButton(
        icon: const Icon(Icons.zoom_in),
        onPressed: () {
          setState(() {
            mapController.move(mapController.center, mapController.zoom + 1);
          });
        },
      ),
      IconButton(
        icon: const Icon(Icons.zoom_out),
        onPressed: () {
          setState(() {
            mapController.move(mapController.center, mapController.zoom - 1);
          });
        },
      ),
      IconButton(
        icon: const Icon(Icons.center_focus_strong),
        onPressed: () {
          setState(() {
            if (markers.isNotEmpty) {
              mapController.fitBounds(_getCenterMapForMarkers(markers));
            }
          });
        },
      )
    ]);
  }
}

// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:data_table_2/data_table_2.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ng1.dart';
import 'package:ngbt_components/ng1/label_amount.dart';
import 'package:ngbt_components/ng1/label_denomination.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:provider/provider.dart';

// Project imports:
import '/l10n/app_localizations.g.dart';
import '/server_session.dart';
import 'image_utils.dart';

/// Signature for [getColumnId] callback.
typedef GetColumnIdFunc = DevicesTableColumnId Function(int columnIndex);

/// Signature for [getColumnCanSort] callback.
typedef GetColumnCanSortFunc = bool Function(DevicesTableColumnId columnId);

const localizationsPreffix = 'devices_table';

///   - [id] unique identifier for the device
///   - [name] name of the device
///   - [category] Category of device
///   - [capabilities] Capabilities of device
///   - [deviceType] Device type
///   - [physicalType] Physical device type
///   - [vendor] Vendor name
///   - [serialNumber] Serial number if available
///   - [connection] Connection configuration of the device
///   - [status] Current status of the device
///   - [enabled] If device is enabled
///   - [branchId] branch id, None if not assigned
///   - [clients] clients assigned. None if not assigned
///   - [cashType] cash device type
///   - [currencies] List of currencies supported by the device. Currencies are specified as 3 char Iso code. Only for 'TCR' category
///   - [cashUnits] List of cash units. Only for 'TCR' category
///   - [printer] Only available if the device is a printer. Contains printer-specific properties.
///   - [components] Only available if the device is Special Electronic. Contains the list of names of components in the device
/// available columns in this table
enum DevicesTableColumnId {
  name,
  branch,
  address,
  status,
  cashUnits,
  totals,
  actions,
  serialNumber,
  deviceType,
  //category,
}

/// available views
enum DevicesTableView { table, grid }

extension _DevicesTableViewExtension on DevicesTableView {
  /// Returns a String that represents the [DevicesTableView] value.
  String get value => describeEnum(this);

  IconData get icon => _getIcon(this);

  static IconData _getIcon(DevicesTableView value) {
    switch (value) {
      case DevicesTableView.table:
        return Icons.table_rows;
      case DevicesTableView.grid:
        return Icons.grid_view;
    }
  }
}

const _defaultColumns = [
  DevicesTableColumnId.name,
  DevicesTableColumnId.branch,
  DevicesTableColumnId.status,
  DevicesTableColumnId.cashUnits,
  DevicesTableColumnId.totals,
];

class DevicesTable extends StatefulWidget {
  const DevicesTable({
    Key? key,
    this.columns = _defaultColumns,
    this.initialView = DevicesTableView.table,
    this.selectionCallback,
  }) : super(key: key);

  final List<DevicesTableColumnId> columns;
  final DevicesTableView initialView;
  final Function(dynamic item)? selectionCallback;

  @override
  _DevicesTableState createState() => _DevicesTableState();
}

class _DevicesTableState extends State<DevicesTable>
    with TickerProviderStateMixin {
  final ValueNotifier<List<Device>> _devicesNotifier =
      ValueNotifier<List<Device>>([]);

  late TabController _tabController;
  late int _index;
  Device? _selectedDevice;

  void _selectDevice(dynamic item) {
    setState(() {
      _selectedDevice = item != null ? item as Device : item;
    });

    if (widget.selectionCallback != null) {
      widget.selectionCallback!(item);
    }
  }

  @override
  void dispose() {
    _devicesNotifier.dispose();
    _tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _index = widget.initialView == DevicesTableView.table ? 0 : 1;
    _tabController =
        TabController(length: DevicesTableView.values.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ServerSession>(
        builder: (context, session, child) => getWidget(session, context));
  }

  Widget getWidget(ServerSession session, BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      //mainAxisSize: MainAxisSize.max,
      children: [
        //NgbtUiUtils.verticalSeparator(),
        Container(
          padding:
              const EdgeInsets.symmetric(vertical: NgbtUiUtils.smallPadding),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: NgbtUiUtils.defaultRadius,
              topRight: NgbtUiUtils.defaultRadius,
            ),
            color: Theme.of(context).dividerColor,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            /*children: [
              _getButton("Table", Icons.table_rows, 0),
              NgbtUiUtils.horizontalSeparator(),
              _getButton("List", Icons.grid_view, 1),
            ],*/
            children:
                DevicesTableView.values.map((v) => _getButton(v)).toList(),
          ),
        ),
        Expanded(
          child: IndexedStack(
            index: _index,
            children: [
              _getTableWidget(session, context),
              _getGridWidget(session, context),
            ],
          ),
        ),
      ],
    );
  }

  Widget _getButton(DevicesTableView view) => Padding(
        padding:
            const EdgeInsets.symmetric(horizontal: NgbtUiUtils.defaultPadding),
        child: ElevatedButton.icon(
          label: Text(AppLocalizations.of(context)!
              .byId('${localizationsPreffix}_view_${view.value}')),
          icon: Icon(view.icon),
          onPressed: () {
            setState(() {
              _index = view.index;
            });
          },
          style: ElevatedButton.styleFrom(
            primary: _index == view.index
                ? Theme.of(context).colorScheme.primary
                : Theme.of(context).primaryColorLight,
            onPrimary: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
      );

  Widget _getGridWidget(ServerSession session, BuildContext context) {
    var sortedDevices = List.from(session.deviceList);
    sortedDevices.sort((a, b) => NgbtUtils.compare(
        (b as Device).status?.name?.severity,
        (a as Device).status?.name?.severity));

    return NgbtGridBuilder(
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 250,
          childAspectRatio: 4 / 2,
          crossAxisSpacing: NgbtUiUtils.smallPadding,
          mainAxisSpacing: NgbtUiUtils.defaultPadding),
      headerTitle: AppLocalizations.of(context)!.devices_table_header_title,
      headerSubtitle: AppLocalizations.of(context)!
          .chars_device_status_devices(sortedDevices.length),
      data: sortedDevices,
      getWidgetCallback: (item) => DeviceTile(
        device: item as Device,
        selected: item == _selectedDevice,
      ),
      selectedCallback: _selectDevice,
      selectedItem: _selectedDevice,
      headerFilter: AppLocalizations.of(context)!
          .byId('${localizationsPreffix}_header_filter'),
      filterEntryCallback: _gridBuilderFilterEntryCallback,
    );
  }

  bool _gridBuilderFilterEntryCallback(
      List<String> filter, dynamic item, String locale) {
    var dev = item as Device;

    if (NgbtUtils.filterMatched(filter, dev.name)) {
      return true;
    }

    if (NgbtUtils.filterMatched(filter, dev.branchName)) {
      return true;
    }

    if (NgbtUtils.filterMatched(filter, dev.branchAddress)) {
      return true;
    }

    if (NgbtUtils.filterMatched(filter, dev.deviceType)) {
      return true;
    }

    if (NgbtUtils.filterMatched(filter, dev.serialNumber)) {
      return true;
    }

    DeviceStatusValue statusValue = dev.statusSafe;
    var text = AppLocalizations.of(context)!
        .byId('device_status_value_${statusValue.name}');

    if (NgbtUtils.filterMatched(filter, text)) {
      return true;
    }

    if (NgbtUtils.filterMatched(
        filter, AmountLabel.getText(dev.amount, locale))) {
      return true;
    }

    return false;
  }

  Widget _getTableWidget(ServerSession session, BuildContext context) {
    var columns = _getColumns(context);

    return NgbtTable(
      headerTitle: AppLocalizations.of(context)!
          .byId('${localizationsPreffix}_header_title'),
      headerSubtitle: AppLocalizations.of(context)!
          .chars_device_status_devices(session.deviceList.length),
      headerFilter: AppLocalizations.of(context)!
          .byId('${localizationsPreffix}_header_filter'),
      headerColumns: AppLocalizations.of(context)!
          .byId('${localizationsPreffix}_select_columns'),
      columns: columns,
      data: session.deviceList,
      getWidgets: _getWidgets,
      filterEntryCallback: _filterEntryCallback,
      sortEntryCallback: _sortEntryCallback,
      paginated: true,
      minWidth: 600,
      selectedCallback: _selectDevice,
      selectedItem: _selectedDevice,
    );
  }

  List<NgbtColumn> _getColumns(BuildContext context) {
    List<NgbtColumn<DevicesTableColumnId>> columns = [];

    columns.add(_createColumn(context, DevicesTableColumnId.deviceType,
        aligment: Alignment.centerRight, columnSize: ColumnSize.S));

    columns.add(_createColumn(context, DevicesTableColumnId.name,
        columnSize: ColumnSize.M));
    columns.add(_createColumn(context, DevicesTableColumnId.branch,
        columnSize: ColumnSize.S));
    columns.add(_createColumn(context, DevicesTableColumnId.address));
    columns.add(_createColumn(context, DevicesTableColumnId.status,
        aligment: Alignment.center));
    columns.add(_createColumn(context, DevicesTableColumnId.cashUnits,
        aligment: Alignment.center, canSort: false));
    columns.add(_createColumn(context, DevicesTableColumnId.totals,
        aligment: Alignment.centerRight, columnSize: ColumnSize.S));
    columns.add(_createColumn(context, DevicesTableColumnId.actions,
        aligment: Alignment.centerRight,
        columnSize: ColumnSize.M,
        canSort: false));
    columns.add(_createColumn(context, DevicesTableColumnId.serialNumber,
        columnSize: ColumnSize.S));

    return columns;
  }

  NgbtColumn<DevicesTableColumnId> _createColumn(
    BuildContext context,
    DevicesTableColumnId id, {
    Alignment aligment = Alignment.centerLeft,
    ColumnSize columnSize = ColumnSize.L,
    bool canSort = true,
  }) {
    var col = NgbtColumn(
        id,
        AppLocalizations.of(context)!
            .byId('${localizationsPreffix}_${id.name}'));

    col.aligment = aligment;
    col.columnSize = columnSize;
    col.canSort = canSort;
    col.visible = widget.columns.contains(id);

    return col;
  }

  /// filter [dev] based on a specific [filter] list
  bool _filterEntryCallback(List<String> filter, dynamic data,
      List<NgbtColumn> columns, String locale) {
    var dev = data as Device;

    bool isValidColumn(List<NgbtColumn> columns, DevicesTableColumnId id) {
      try {
        columns.firstWhere((c) => c.id == id);
        return true;
      } on StateError {
        return false;
      }
    }

    if (isValidColumn(columns, DevicesTableColumnId.name) &&
        NgbtUtils.filterMatched(filter, dev.name)) {
      return true;
    }

    if (isValidColumn(columns, DevicesTableColumnId.branch) &&
        NgbtUtils.filterMatched(filter, dev.branchName)) {
      return true;
    }

    if (isValidColumn(columns, DevicesTableColumnId.address) &&
        NgbtUtils.filterMatched(filter, dev.branchAddress)) {
      return true;
    }

    if (isValidColumn(columns, DevicesTableColumnId.serialNumber) &&
        NgbtUtils.filterMatched(filter, dev.serialNumber)) {
      return true;
    }

    if (isValidColumn(columns, DevicesTableColumnId.deviceType) &&
        NgbtUtils.filterMatched(filter, dev.deviceType)) {
      return true;
    }

    if (isValidColumn(columns, DevicesTableColumnId.status)) {
      DeviceStatusValue statusValue = dev.statusSafe;

      var text = AppLocalizations.of(context)!
          .byId('device_status_value_${statusValue.name}');

      if (NgbtUtils.filterMatched(filter, text)) {
        return true;
      }
    }

    if (isValidColumn(columns, DevicesTableColumnId.totals) &&
        NgbtUtils.filterMatched(
            filter, AmountLabel.getText(dev.amount, locale))) {
      return true;
    }

    return false;
  }

  /// sort [devices] based on current settings [_sortColumnIndex] and [_sortAscending]
  int _sortEntryCallback(dynamic _a, dynamic _b, NgbtColumn column) {
    var a = _a as Device;
    var b = _b as Device;

    if (column.id is DevicesTableColumnId) {
      var columnId = (column.id as DevicesTableColumnId);
      if (columnId == DevicesTableColumnId.totals) {
        return (a.amount?.value ?? 0) - (b.amount?.value ?? 0);
      } else if (columnId == DevicesTableColumnId.status) {
        return NgbtUtils.compare(
            b.status?.name?.severity, a.status?.name?.severity);
      } else if (columnId == DevicesTableColumnId.address) {
        return NgbtUtils.compare(a.branchAddress, b.branchAddress);
      } else if (columnId == DevicesTableColumnId.branch) {
        return NgbtUtils.compare(a.branchName, b.branchName);
      } else if (columnId == DevicesTableColumnId.serialNumber) {
        return NgbtUtils.compare(a.serialNumber, b.serialNumber);
      } else if (columnId == DevicesTableColumnId.deviceType) {
        return NgbtUtils.compare(a.deviceType, b.deviceType);
      } else if (columnId == DevicesTableColumnId.name) {
        return NgbtUtils.compare(a.name, b.name);
      }
    }
    return 0;
  }

  ///
  List<Widget> _getWidgets(
      NgbtColumn column, dynamic rowData, BuildContext context) {
    Device device = rowData as Device;
    var columnId = column.id as DevicesTableColumnId;
    var locale = Localizations.localeOf(context).toString();

    switch (columnId) {
      case DevicesTableColumnId.actions:
        return _getActions(device);
      case DevicesTableColumnId.totals:
        return _getTotal(device);
      case DevicesTableColumnId.cashUnits:
        return _getCashunits(device, locale);
      case DevicesTableColumnId.status:
        return _getStatus(device);
      case DevicesTableColumnId.address:
        return _getText(device.branchAddress);
      case DevicesTableColumnId.branch:
        return _getBranch(device);
      case DevicesTableColumnId.serialNumber:
        return _getText(device.serialNumber);
      case DevicesTableColumnId.deviceType:
        return _getText(device.deviceType);
      case DevicesTableColumnId.name:
        return _getName(device);
    }
  }

  List<Widget> _getText(String? text) {
    return [Flexible(child: Text(text ?? ""))];
  }

  List<Widget> _getTotal(Device device) =>
      [Flexible(child: AmountLabel(amount: device.amount))];

  List<Widget> _getName(Device device) {
    return [
      Flexible(
          flex: 1,
          child: Container(
            padding: const EdgeInsets.all(0),
            height: 40,
            width: 40,
            child: ImageUtils.getDeviceTypeImage(device, height: 40),
          )),
      Flexible(
        flex: 4,
        child: Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: NgbtUiUtils.defaultPadding),
          child: Text(
            device.name!,
          ),
        ),
      ),
    ];
  }

  List<Widget> _getBranch(Device device) {
    return [
      Flexible(
          child: Text(device.branchName ??
              AppLocalizations.of(context)!
                  .byId('${localizationsPreffix}_branch_not_asigned')))
    ];
  }

  List<Widget> _getStatus(Device device) {
    DeviceStatusValue statusValue = device.statusSafe;

    // ignore: unused_local_variable
    int severity = statusValue.severity;

    return [
      Chip(
          backgroundColor: statusValue.color,
          labelStyle:
              TextStyle(color: NgbtUiUtils.getContrastColor(statusValue.color)),
          label: Text(
            AppLocalizations.of(context)!
                .byId('device_status_value_${statusValue.name}'),
            textAlign: TextAlign.center,
            maxLines: 3,
          ))
    ];
  }

  List<Widget> _getCashunits(Device device, String locale) {
    return device.cashUnits != null
        ? device.cashUnits!
            .map((cu) => Flexible(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: NgbtUiUtils.smallPadding),
                    child: Tooltip(
                      // TODO background color if status != CashUnitStatus.OK
                      child: Text(cu.physicalName!),
                      message: _getCashUnitTooltip(cu, locale),
                    ),
                  ),
                ))
            .toList()
        : [];
  }

  String _getCashUnitTooltip(CashUnit cu, String locale) {
    List<String> list = [];

    if (cu.status != null && cu.status != CashUnitStatus.OK) {
      list.add('Status: ' + cu.status!.name);
    }

    if (cu.denominations != null) {
      cu.denominations!.forEach((key, value) {
        list.add(DenominationLabel.getText(Denomination.fromKey(key), locale) +
            ' x$value');
      });
    }

    return list.join("\n");
  }

  List<Widget> _getActions(Device device) {
    return [
      Flexible(
          child: ActionChip(
        onPressed: () => {},
        avatar: const Icon(
          Icons.update,
          //color: Colors.black54,
        ),
        label: const Text("fake_1.2.3"),
        tooltip: "Update firmware",
      )),
      const Flexible(
          child: IconButton(
        icon: Icon(Icons.download),
        tooltip: "Download logs",
        onPressed: null,
      )),
    ];
  }
}

class DeviceTile extends StatelessWidget {
  const DeviceTile({
    Key? key,
    required this.device,
    required this.selected,
  }) : super(key: key);

  final Device device;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    var statusValue = device.statusSafe;
    EdgeInsets cardInsets = const EdgeInsets.fromLTRB(20, 5, 0, 5);
    double statusSeparatorHeight = 2;
    double statusSeparatorWidth = 80;
    double imageWidth = 70;
    double imageHeight = 140;
    EdgeInsets cardContentInsets =
        EdgeInsets.fromLTRB(imageWidth - cardInsets.left + 10, 6, 3, 6);

    var deviceImage =
        ImageUtils.getDeviceTypeImage(device, height: imageHeight.toInt());

    final deviceThumbnail = Container(
      height: imageHeight,
      width: imageWidth,
      alignment: FractionalOffset.centerRight,
      //child: FittedBox(child: deviceImage, fit: BoxFit.scaleDown),
      child: deviceImage,
    );

    var headerStyle = Theme.of(context).textTheme.bodyText1!.copyWith(
          color: statusValue.color.darken(50),
        );
    var subheaderStyle = Theme.of(context).textTheme.bodyText2!.copyWith(
          color: statusValue.color.lighten(30),
        );
    var statusStyle = Theme.of(context).textTheme.subtitle1!.copyWith(
          color: statusValue.color.lighten(50),
        );

    var deviceCardContent = Container(
      margin: cardContentInsets,
      constraints: const BoxConstraints.expand(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: Text(
              device.serialNumber ?? device.name ?? "?",
              style: headerStyle,
            ),
          ),
          const SizedBox(height: 4),
          Flexible(
            child: AmountLabel(
              amount: device.amount,
              style: subheaderStyle,
            ),
          ),
          Flexible(
            child: Text(device.deviceType ?? device.physicalType ?? "?",
                style: subheaderStyle),
          ),
          Container(
            margin: const EdgeInsets.only(top: 6, bottom: 2),
            height: statusSeparatorHeight,
            width: statusSeparatorWidth,
            color: statusValue.color.lighten(20),
          ),
          Flexible(
            child: Text(
              AppLocalizations.of(context)!
                  .byId('device_status_value_${statusValue.name}'),
              style: statusStyle,
            ),
          ),
        ],
      ),
    );

    final deviceCard = Container(
      child: deviceCardContent,
      margin: cardInsets,
      decoration: BoxDecoration(
        color: statusValue.color,
        shape: BoxShape.rectangle,
        borderRadius: NgbtUiUtils.defaultBorderRadius,
        boxShadow: <BoxShadow>[
          // default selection identification on gridbuilder
          /*BoxShadow(
            color: Theme.of(context).colorScheme.secondary,
            spreadRadius: selected ? 6 : 0,
            blurRadius: selected ? 2.0 : 0,
          ),*/
          BoxShadow(
            color: selected ? Colors.transparent : Colors.black12,
            blurRadius: 2.0,
            offset: const Offset(4.0, 4.0),
          ),
        ],
      ),
    );

    return Container(
        //height: 180.0,
        margin: const EdgeInsets.symmetric(horizontal: 8),
        child: Stack(
          children: <Widget>[
            deviceCard,
            deviceThumbnail,
          ],
        ));
  }
}

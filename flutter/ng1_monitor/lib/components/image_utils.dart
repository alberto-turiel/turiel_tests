// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:ng1_null_safe/ng1_null_safe.dart';

class ImageUtils {
  static final _deviceTypesMap = <String, Image>{};
  static Image getDeviceTypeImage(Device device, {int? height, int? width}) {
    String deviceType = device.deviceType == null
        ? "unknown"
        : device.deviceType!.toLowerCase().replaceAll(" ", "_");
    String name =
        "${device.vendor != null ? device.vendor!.toLowerCase() : ''}-$deviceType";

    String key = 'h${height ?? '-'}_${width ?? '-'}_$name';

    if (_deviceTypesMap.containsKey(key)) {
      return _deviceTypesMap[key]!;
    } else {
      var image = Image.asset(
        "assets/images/devices/$name.png",
        cacheHeight: height,
        cacheWidth: width,
        errorBuilder:
            (BuildContext context, Object exception, StackTrace? stackTrace) {
          return Image.asset(
            "assets/images/devices/unknown.png",
            cacheHeight: height,
            cacheWidth: width,
          );
        },
      );
      _deviceTypesMap[key] = image;

      return image;
    }
  }
}

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import '/server_session.dart';

class ServerDisconnect extends StatelessWidget {
  const ServerDisconnect({Key? key}) : super(key: key);

  Widget getWidget(ServerSession session, BuildContext context) {
    switch (session.status) {
      case ServerSessionStatus.notConnected:
      case ServerSessionStatus.connecting:
      case ServerSessionStatus.disconnecting:
        return const SizedBox.shrink();
      case ServerSessionStatus.connected:
        return PopupMenuButton(
          icon: const Icon(Icons.link_off),
          tooltip:
              AppLocalizations.of(context)!.server_connect_disconnect + "...",
          itemBuilder: (context) => [
            PopupMenuItem(
              child:
                  Text(AppLocalizations.of(context)!.server_connect_disconnect),
              value: true,
            )
          ],
          onSelected: (bool disconnect) {
            if (disconnect) {
              session.disconnectFromServer();
            }
          },
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ServerSession>(
        builder: (context, session, child) => getWidget(session, context));
  }
}

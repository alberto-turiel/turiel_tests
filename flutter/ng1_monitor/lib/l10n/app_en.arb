{
	"@@locale": "en",
	"language_name": "English",
    "@language_name": {
		"description": "translator help text"
	},
	"home_title": "NG1 Monitor",

    "menu_dashboard": "Dashboard",
    "menu_device_list": "Device list",
    "menu_device_details": "Device details",
    "menu_settings": "Settings",

    "settings_language": "Language",
    "settings_theme": "Theme",
    "settings_theme_mode": "Selected theme: {mode}",
    "@settings_theme_mode": {
		"placeholders": {
			"mode": {
				"type": "String"
			}
		}
	},
    "settings_theme_scheme": "Selected Scheme: {scheme}",
    "@settings_theme_scheme": {
		"placeholders": {
			"scheme": {
				"type": "String"
			}
		}
	},
    "settings_about": "More info...",

    "all_network": "Network",

    "server_connect_connect": "Connect to NG1 server",
    "server_connect_connecting": "Connecting...",
    "server_connect_disconnect": "Disconnect",
    "server_connect_ip": "NG1 server hostname",
    "server_connect_ip_error": "Please enter a valid hostname or IP address",
    "server_connect_port": "Port",
    "server_connect_port_error": "Please enter a valid Port",
    "server_connect_user": "User",
    "server_connect_user_error": "Please enter a user name",
    "server_connect_password": "Password",
    "server_connect_password_error": "Please enter user password",
    "server_connect_error_UNKNOWN": "Can't connect",
    "server_connect_error_ERROR_BAD_USERNAME": "Please check user and/or password",
    "server_connect_error_ERROR_BAD_PASSWORD": "Please check user and/or password",
    "server_connect_error_WEBSOCKET_INVALID_PORT": "Invalid port",
    "server_connect_error_CONNECTION_BROKEN": "Please check hostname",
    "server_connect_error_CONNECTION_CLOSED": "NG1 server connection closed",
    "server_connect_error_WEBSOCKET_ERROR": "WebSocket error",
    "server_connect_error_ERROR_METHOD_UNKNOWN": "Monitor service is not enabled",

    "serverSessionStatus_connecting": "Conecting to {ip}:{port}...",
    "@serverSessionStatus_connecting": {
		"placeholders": {
			"ip": {
				"type": "String"
			},
			"port": {}
		}
	},
    "serverSessionStatus_connected": "Conected to {ip}:{port}",
    "@serverSessionStatus_connected": {
		"placeholders": {
			"ip": {
				"type": "String"
			},
			"port": {}
		}
	},
    "serverSessionStatus_disconnecting": "Disconecting from {ip}:{port}...",
    "@serverSessionStatus_disconnecting": {
		"placeholders": {
			"ip": {
				"type": "String"
			},
			"port": {}
		}
	},
    "serverSessionStatus_notConnected": "Not connected",


    "device_status_value_UNKNOWN": "Unknown",
    "device_status_value_INITIALIZING":  "Initializing",
    "device_status_value_ONLINE_WORKING":  "Working",
    "device_status_value_ONLINE_INOPERABLE":  "Inoperable",
    "device_status_value_ONLINE_UNLICENSED":  "Unlicensed",
    "device_status_value_OFFLINE":  "Offline",
    "device_status_value_DISABLED":  "Disabled",

    "cash_transaction_type_value_BLIND_DEPOSIT": "Blind deposit",
    "cash_transaction_type_value_TARGET_DEPOSIT": "Target deposit",
    "cash_transaction_type_value_LOAD": "Load",
    "cash_transaction_type_value_WITHDRAWAL": "Withdrawal",
    "cash_transaction_type_value_UNLOAD": "Unload",
    "cash_transaction_type_value_EMPTY": "Empty",
    "cash_transaction_type_value_CLEAR_MISMATCH": "Clear mismatch",
    "cash_transaction_type_value_COUNTER_CORRECTION": "Counter correction",
    "cash_transaction_type_value_REPLACE_CASH_UNIT": "Replace cash unit",
    "cash_transaction_type_value_REPLENISH": "Replenish",
    "cash_transaction_type_value_BILL_BREAKING": "Bill Breaking",
    "cash_transaction_type_value_COUNT": "Count",
    "cash_transaction_type_value_SORT": "Sort",
    "cash_transaction_type_value_UNKNOWN": "Unknown",

    "devices_table_header_title": "Devices",
    "devices_table_header_filter": "Filter devices ...",
    "devices_table_select_columns": "Select columns ...",
    "devices_table_name": "Name",
    "devices_table_branch": "Branch",
    "devices_table_address": "Address",
    "devices_table_status": "Status",
    "devices_table_cashUnits": "Cash units",
    "devices_table_totals": "Totals",
    "devices_table_serialNumber": "Serial Number",
    "devices_table_deviceType": "Device Type",
    "devices_table_actions": "Actions",
    "devices_table_branch_not_asigned": "Not assigned",
    "devices_table_view_table": "Table",
    "devices_table_view_grid": "Grid",

    "branch_map_title": "Branch locations",

    "journal_table_header_title": "Journal",
    "journal_table_no_data": "No Data",
    "journal_table_filter_transactions": "Transactions",
    "journal_table_header_filter": "Filter entries ...",
    "journal_table_select_columns": "Select columns ...",
    "journal_table_timestamp": "Time",
    "journal_table_user": "User",
    "journal_table_transaction_type": "Trans. Type",
    "journal_table_amount":"Amount",
    "journal_table_device_amount":"Device amount",
    "journal_table_transaction_result":"Result",
    "journal_table_branch":"Branch",
    "journal_table_message_type":"Message type",
    "journal_table_message":"Message",
    "journal_table_transaction_id":"Trans. Id",
    "journal_table_workstation":"Workstation",

    "mix_from_device": "Total",
    "mix_table_no_data": "No Data",
    "mix_table_header_title": "Denominations",
    "mix_table_denomination": "Denomination",
    "mix_table_items": "Notes",
    "mix_table_amount": "Amount",
    "mix_table_quality": "Quality",

    "cash_units_table_header_title": "Cash units",
    "cash_units_table_no_data": "No Data",
    "cash_units_table_physicalName": "Name",
    "cash_units_table_denominations": "Denominations",
    "cash_units_table_amount": "Amount",
    "cash_units_table_items": "Notes",

    "notes_counter": "{count,plural, =0{no notes} =1{one note} other{{count} notes}}",
    "@notes_counter": {
        "placeholders": {
            "count": {
                "type": "int",
                "format": "decimalPattern"
            }    
        }    
    },    
    
    "chars_no_data": "No Data",

    "chars_device_status_title": "Status",
    "chars_device_status_devices": "{count,plural, =0{no devices} =1{one device} other{{count} devices}}",
    "@chars_device_status_devices": {
        "placeholders": {
            "count": {
                "type": "int",
                "format": "decimalPattern"
            }    
        }    
    },    

    "chars_device_transactions_title": "Transactions",
    "chars_device_transactions_total": "{count,plural, =0{no transactions} =1{one transaction} other{{count} transactions}}",
    "@chars_device_transactions_total": {
        "placeholders": {
            "count": {
                "type": "int",
                "format": "decimalPattern"
            }    
        }    
    }

}

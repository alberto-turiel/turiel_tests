// GENERATED CODE - DO NOT MODIFY BY HAND

// part of 'app_localizations.dart';

// **************************************************************************
// L10nGenerator
// **************************************************************************

/// 2021-10-20 @ 13:05 - Processing /ng1_monitor/lib/l10n/app_localizations.dart

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

extension AppLocalizationsEntensions on AppLocalizations {
  /// get translation of the specific [id] in current languague
  /// If [positionalArguments] is null, it's considered an empty list.
  String byId(String id, [List<dynamic>? positionalArguments]) {
    var translations = AppLocalizationsEntensions._get(this);
    var f = translations[id];
    String retValue = '$id???';
    try {
      if (f != null) {
        retValue = Function.apply(f, positionalArguments);
      }
    } on Exception catch (ex) {
      // ignore: avoid_print
      print(ex.toString());
    }
    return retValue;
  }

  ///
  static final Map<String, Map<String, Function>> _cached = {};

  static Map<String, Function> _get(AppLocalizations al) {
    if (_cached[al.localeName] == null) {
      _cached[al.localeName] = _load(al);
    }
    return _cached[al.localeName]!;
  }

  static Map<String, Function> _load(AppLocalizations al) {
    Map<String, Function> map = {
      'localeName': () => al.localeName,
      'language_name': () => al.language_name,
      'home_title': () => al.home_title,
      'menu_dashboard': () => al.menu_dashboard,
      'menu_device_list': () => al.menu_device_list,
      'menu_device_details': () => al.menu_device_details,
      'menu_settings': () => al.menu_settings,
      'settings_language': () => al.settings_language,
      'settings_theme': () => al.settings_theme,
      'settings_about': () => al.settings_about,
      'all_network': () => al.all_network,
      'server_connect_connect': () => al.server_connect_connect,
      'server_connect_connecting': () => al.server_connect_connecting,
      'server_connect_disconnect': () => al.server_connect_disconnect,
      'server_connect_ip': () => al.server_connect_ip,
      'server_connect_ip_error': () => al.server_connect_ip_error,
      'server_connect_port': () => al.server_connect_port,
      'server_connect_port_error': () => al.server_connect_port_error,
      'server_connect_user': () => al.server_connect_user,
      'server_connect_user_error': () => al.server_connect_user_error,
      'server_connect_password': () => al.server_connect_password,
      'server_connect_password_error': () => al.server_connect_password_error,
      'server_connect_error_UNKNOWN': () => al.server_connect_error_UNKNOWN,
      'server_connect_error_ERROR_BAD_USERNAME': () =>
          al.server_connect_error_ERROR_BAD_USERNAME,
      'server_connect_error_ERROR_BAD_PASSWORD': () =>
          al.server_connect_error_ERROR_BAD_PASSWORD,
      'server_connect_error_WEBSOCKET_INVALID_PORT': () =>
          al.server_connect_error_WEBSOCKET_INVALID_PORT,
      'server_connect_error_CONNECTION_BROKEN': () =>
          al.server_connect_error_CONNECTION_BROKEN,
      'server_connect_error_CONNECTION_CLOSED': () =>
          al.server_connect_error_CONNECTION_CLOSED,
      'server_connect_error_WEBSOCKET_ERROR': () =>
          al.server_connect_error_WEBSOCKET_ERROR,
      'server_connect_error_ERROR_METHOD_UNKNOWN': () =>
          al.server_connect_error_ERROR_METHOD_UNKNOWN,
      'serverSessionStatus_notConnected': () =>
          al.serverSessionStatus_notConnected,
      'device_status_value_UNKNOWN': () => al.device_status_value_UNKNOWN,
      'device_status_value_INITIALIZING': () =>
          al.device_status_value_INITIALIZING,
      'device_status_value_ONLINE_WORKING': () =>
          al.device_status_value_ONLINE_WORKING,
      'device_status_value_ONLINE_INOPERABLE': () =>
          al.device_status_value_ONLINE_INOPERABLE,
      'device_status_value_ONLINE_UNLICENSED': () =>
          al.device_status_value_ONLINE_UNLICENSED,
      'device_status_value_OFFLINE': () => al.device_status_value_OFFLINE,
      'device_status_value_DISABLED': () => al.device_status_value_DISABLED,
      'cash_transaction_type_value_BLIND_DEPOSIT': () =>
          al.cash_transaction_type_value_BLIND_DEPOSIT,
      'cash_transaction_type_value_TARGET_DEPOSIT': () =>
          al.cash_transaction_type_value_TARGET_DEPOSIT,
      'cash_transaction_type_value_LOAD': () =>
          al.cash_transaction_type_value_LOAD,
      'cash_transaction_type_value_WITHDRAWAL': () =>
          al.cash_transaction_type_value_WITHDRAWAL,
      'cash_transaction_type_value_UNLOAD': () =>
          al.cash_transaction_type_value_UNLOAD,
      'cash_transaction_type_value_EMPTY': () =>
          al.cash_transaction_type_value_EMPTY,
      'cash_transaction_type_value_CLEAR_MISMATCH': () =>
          al.cash_transaction_type_value_CLEAR_MISMATCH,
      'cash_transaction_type_value_COUNTER_CORRECTION': () =>
          al.cash_transaction_type_value_COUNTER_CORRECTION,
      'cash_transaction_type_value_REPLACE_CASH_UNIT': () =>
          al.cash_transaction_type_value_REPLACE_CASH_UNIT,
      'cash_transaction_type_value_REPLENISH': () =>
          al.cash_transaction_type_value_REPLENISH,
      'cash_transaction_type_value_BILL_BREAKING': () =>
          al.cash_transaction_type_value_BILL_BREAKING,
      'cash_transaction_type_value_COUNT': () =>
          al.cash_transaction_type_value_COUNT,
      'cash_transaction_type_value_SORT': () =>
          al.cash_transaction_type_value_SORT,
      'cash_transaction_type_value_UNKNOWN': () =>
          al.cash_transaction_type_value_UNKNOWN,
      'devices_table_header_title': () => al.devices_table_header_title,
      'devices_table_header_filter': () => al.devices_table_header_filter,
      'devices_table_select_columns': () => al.devices_table_select_columns,
      'devices_table_name': () => al.devices_table_name,
      'devices_table_branch': () => al.devices_table_branch,
      'devices_table_address': () => al.devices_table_address,
      'devices_table_status': () => al.devices_table_status,
      'devices_table_cashUnits': () => al.devices_table_cashUnits,
      'devices_table_totals': () => al.devices_table_totals,
      'devices_table_serialNumber': () => al.devices_table_serialNumber,
      'devices_table_deviceType': () => al.devices_table_deviceType,
      'devices_table_actions': () => al.devices_table_actions,
      'devices_table_branch_not_asigned': () =>
          al.devices_table_branch_not_asigned,
      'devices_table_view_table': () => al.devices_table_view_table,
      'devices_table_view_grid': () => al.devices_table_view_grid,
      'branch_map_title': () => al.branch_map_title,
      'journal_table_header_title': () => al.journal_table_header_title,
      'journal_table_no_data': () => al.journal_table_no_data,
      'journal_table_filter_transactions': () =>
          al.journal_table_filter_transactions,
      'journal_table_header_filter': () => al.journal_table_header_filter,
      'journal_table_select_columns': () => al.journal_table_select_columns,
      'journal_table_timestamp': () => al.journal_table_timestamp,
      'journal_table_user': () => al.journal_table_user,
      'journal_table_transaction_type': () => al.journal_table_transaction_type,
      'journal_table_amount': () => al.journal_table_amount,
      'journal_table_device_amount': () => al.journal_table_device_amount,
      'journal_table_transaction_result': () =>
          al.journal_table_transaction_result,
      'journal_table_branch': () => al.journal_table_branch,
      'journal_table_message_type': () => al.journal_table_message_type,
      'journal_table_message': () => al.journal_table_message,
      'journal_table_transaction_id': () => al.journal_table_transaction_id,
      'journal_table_workstation': () => al.journal_table_workstation,
      'mix_from_device': () => al.mix_from_device,
      'mix_table_no_data': () => al.mix_table_no_data,
      'mix_table_header_title': () => al.mix_table_header_title,
      'mix_table_denomination': () => al.mix_table_denomination,
      'mix_table_items': () => al.mix_table_items,
      'mix_table_amount': () => al.mix_table_amount,
      'mix_table_quality': () => al.mix_table_quality,
      'cash_units_table_header_title': () => al.cash_units_table_header_title,
      'cash_units_table_no_data': () => al.cash_units_table_no_data,
      'cash_units_table_physicalName': () => al.cash_units_table_physicalName,
      'cash_units_table_denominations': () => al.cash_units_table_denominations,
      'cash_units_table_amount': () => al.cash_units_table_amount,
      'cash_units_table_items': () => al.cash_units_table_items,
      'chars_no_data': () => al.chars_no_data,
      'chars_device_status_title': () => al.chars_device_status_title,
      'chars_device_transactions_title': () =>
          al.chars_device_transactions_title,
      'settings_theme_mode': (String mode) => al.settings_theme_mode(mode),
      'settings_theme_scheme': (String scheme) =>
          al.settings_theme_scheme(scheme),
      'serverSessionStatus_connecting': (String ip, Object port) =>
          al.serverSessionStatus_connecting(ip, port),
      'serverSessionStatus_connected': (String ip, Object port) =>
          al.serverSessionStatus_connected(ip, port),
      'serverSessionStatus_disconnecting': (String ip, Object port) =>
          al.serverSessionStatus_disconnecting(ip, port),
      'notes_counter': (int count) => al.notes_counter(count),
      'chars_device_status_devices': (int count) =>
          al.chars_device_status_devices(count),
      'chars_device_transactions_total': (int count) =>
          al.chars_device_transactions_total(count),
    };
    return map;
  }
}

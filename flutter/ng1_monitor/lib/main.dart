// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart' show Currencies;
import 'package:ngbt_components/ngbt_components.dart';
import 'package:ngbt_components/ngbt_locale.dart';
import 'package:ngbt_components/ngbt_theme.dart';
import 'package:ngbt_log/ngbt_log.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

// Project imports:
import 'constants.dart';
import 'screens/main_layout.dart';
import 'server_session.dart';

void main() {
  //
  NgbtLogger.activate();
  NgbtLogger.setLevel(NgbtLogger.DEBUG);

  ngbtSetDefaultLocale("en");
  runApp(const ProviderScope(child: ThemedApp()));
}

class ThemedApp extends ConsumerWidget {
  const ThemedApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var currentLocale = ref.watch(ngbtLocaleProvider);
    var currentTheme = ref.watch(ngbtThemeProvider);

    return StatefulWrapper(
      onInit: () {
        NgbtLogger.start(console: false);

        var extraLogger = NgbtLoggerFile(
          fileName: 'session',
          filters: [
            ServerSession.loggerName,
            "NG1.Session",
          ],
        );
        NgbtLogger.addOutput(extraLogger);

        /// NG1: Load currencies
        Currencies.initFromFuture(
            rootBundle.loadString('assets/currencies.json'));
      },
      child: Builder(
        builder: (context) => MaterialApp(
          debugShowCheckedModeBanner: false,
          title: AppConst.appName,

          //localization
          locale: currentLocale,
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,

          // NGBT theming
          theme: NgbtTheme.ngbtLightTheme(currentTheme.themeIndex),
          darkTheme: NgbtTheme.ngbtDarkTheme(currentTheme.themeIndex),
          themeMode: currentTheme.themeMode,
          // This simple example app has only one page.
          home: const SessionProvider(child: MainLayout()),
        ),
      ),
    );
  }
}

// ignore_for_file: avoid_print

// Dart imports:
import 'dart:convert';
import 'dart:io';
import 'dart:math';

// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:latlong2/latlong.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_log/ngbt_log.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'l10n/app_localizations.g.dart';

class SessionProvider extends StatelessWidget {
  final Widget child;

  const SessionProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ServerSession(),
      child: child,
    );
  }
}

enum ServerSessionStatus {
  connecting,
  connected,
  disconnecting,
  notConnected,
}

Map<int, Branch> _branches = <int, Branch>{};
Map<int, LatLng> _branchCoords = <int, LatLng>{};

// speed up device amounts calculation by doing it just once
Map<int, Amount?> _deviceAmounts = {};

Amount? _getDeviceAmount(Device dev) => dev.id != null
    ? _deviceAmounts.putIfAbsent(dev.id!, () => Amount.fromDevice(dev))
    : null;

clearDeviceAmounts() => _deviceAmounts.clear();

/// utility methods for [Device] class
extension DeviceExtension on Device {
  Amount? get amount => _getDeviceAmount(this);
  DeviceStatusValue get statusSafe => status?.name ?? DeviceStatusValue.UNKNOWN;
  Branch? get branch => _branches[branchId];
  String? get branchName => branch?.name;
  String? get branchAddress => branch?.address;
  LatLng? get branchLatLong => branch?.latLong;
}

/// utility methods for [Branch] class
extension BranchExtension on Branch {
  LatLng? get latLong => _branchCoords[id];
}

extension ServerSessionStatusExtension on ServerSessionStatus {
  /// Returns a String that represents the [CashUnitCounterStatus] value.
  String get value => describeEnum(this);
}

class ServerSession with ChangeNotifier {
  static String loggerName = "SESSION";

  var status = ServerSessionStatus.notConnected;
  String? ip;
  int? port;
  Uri? uri;
  Session? session;
  User? user;
  List<Device> deviceList = [];
  int _subscriptionTransactionID = -1;
  Map<int, Branch> get branches => _branches;

  String _connectionError = "";
  bool get isError => _connectionError.isNotEmpty;
  String get error => _connectionError;

  ServerSession();

  void cleanError() {
    _connectionError = "";
    notifyListeners();
  }

  void disconnectFromServer() async {
    if (status == ServerSessionStatus.connected) {
      // update status
      _statusDisconnecting();

      try {
        await session?.api.monitor
            .unsubscribe(transactionId: _subscriptionTransactionID);
      } catch (ex, stackTrace) {
        NgbtLogger.warning("monitor.unsubscribe",
            loggerName: loggerName, error: ex, stackTrace: stackTrace);
      }

      try {
        await session?.api.user.logoff();
      } catch (ex, stackTrace) {
        NgbtLogger.warning("user.logoff",
            loggerName: loggerName, error: ex, stackTrace: stackTrace);
      }

      try {
        await session?.api.server.disconnect();
      } catch (ex, stackTrace) {
        NgbtLogger.warning("server.disconnect",
            loggerName: loggerName, error: ex, stackTrace: stackTrace);
      }

      // update status
      _statusNotConnected();
    }
  }

  Future<Exception?> connectToServer(
    String ip,
    int port,
    String username,
    String password,
  ) async {
    // update status
    _statusConnecting(ip, port);

    try {
      final session = await ng1_connect(
        uri: uri,
        listener: (e) {
          NgbtLogger.debug("ng1_connect.listener.EVENT: ${e.name}",
              loggerName: loggerName);
          if (e.name == "DEVICE_CHANGED") {
            _deviceChanged(e.data);
          } else if (e.name == "CONNECTION_CLOSED") {
            // update status
            _statusNotConnected(e.name);
          }
        },
      );

      NgbtLogger.info("Connected to server. Login with $username ...",
          loggerName: loggerName);
      final user = await session.api.user.logon(
        username: username,
        password: password,
      );

      //
      const messageTypes = [
        //"DEVICE_CHANGED",
        "TRANSACTION_END",
        "DEVICE_STATUS_CHANGED",
      ];

      NgbtLogger.info("Logged in. Suscribing for events $messageTypes",
          loggerName: loggerName);
      await session.api.monitor.subscribe(
        subscription: MonitorSubscription(messageTypes: messageTypes),
        listener: (e) {
          if (e.name == "MONITOR_NOTIFICATION") {
            MonitorNotification mn = e.data as MonitorNotification;
            if (mn.name == "DEVICE_STATUS_CHANGED") {
              _deviceChanged(e.data);
            } else if (mn.name == "TRANSACTION_END") {
              _deviceChanged(e.data);
            }
          } else if (e.name == "TRANSACTION_START") {
            var ts = e.data as TransactionStart;
            if (ts.transactionType == "MONITOR_SUBSCRIPTION") {
              _subscriptionTransactionID = ts.transactionId;
              NgbtLogger.info(
                  "${ts.transactionType} transactionID: $_subscriptionTransactionID",
                  loggerName: loggerName);
            }
          } else if (e.name == "TRANSACTION_END") {
            var te = e.data as TransactionEnd;
            if (te.transactionType == "MONITOR_SUBSCRIPTION") {
              NgbtLogger.info(
                  "${te.transactionType} transactionID: $_subscriptionTransactionID",
                  loggerName: loggerName);
              _subscriptionTransactionID = -1;
            }
          }
        },
      );

      // update status
      _statusConnected(session, user);
      return null;
    } on Exception catch (ex, stackTrace) {
      if (ex.runtimeType == APIException) {
        NgbtLogger.error("Connect error",
            loggerName: loggerName, error: ex, stackTrace: stackTrace);
      } else {
        NgbtLogger.error("Connect exception",
            loggerName: loggerName, error: ex, stackTrace: stackTrace);
      }
      // update status
      _statusNotConnected();
      return ex;
    }
  }

  Future<void> _setDeviceList(List<Device> list) async {
    deviceList = List<Device>.from(list);
    clearDeviceAmounts();

    /// add test devices
    _addDemoDevices(list);
  }

  void _addDemoDevices(List<Device> list) {
    String _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();

    String getRandomString(int length) =>
        String.fromCharCodes(Iterable.generate(
            length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

    List<String> vendors = ['Arca', 'Glory'];
    Map<String, List<String>> deviceTypes = {
      'Arca': [
        'CM18 Solo Plus',
        'CM18 Solo',
        'CM18B',
        'CM18 EVO',
        'Vero',
        'CM18 Novus'
      ],
      'Glory': ['Vertera 5G', 'Vertera 6G', 'RBG 100', 'CI-10']
    };

    var idx = 1000;
    for (int n = 1; n < 5; n++) {
      for (int i = 0; i < list.length; i++) {
        try {
          var tmpDevice = list[i].toJson();
          tmpDevice['id'] = idx;
          tmpDevice['branch_id'] = _rnd.nextInt(_branches.length);

          tmpDevice['name'] = "${tmpDevice['name']} ${getRandomString(3)} ";
          tmpDevice['serial_number'] = getRandomString(12);
          tmpDevice['vendor'] = vendors[_rnd.nextInt(vendors.length)];
          var devices = deviceTypes[tmpDevice['vendor']]!;
          tmpDevice['device_type'] = devices[_rnd.nextInt(devices.length)];
          tmpDevice['status']['name'] = DeviceStatusValue
              .values[_rnd.nextInt(DeviceStatusValue.values.length)].name;
          deviceList.add(Device.fromJson(tmpDevice));
        } catch (ex, stackTrace) {
          NgbtLogger.warning("DemoDevices Device.fromJson",
              loggerName: loggerName, error: ex, stackTrace: stackTrace);
        }
        idx++;
      }
    }

    NgbtLogger.debug("DemoDevices.length ${deviceList.length}",
        loggerName: loggerName);
  }

  Future<void> _setBranches(List<Branch> branchesList) async {
    _branches.clear();
    _branchCoords.clear();
    for (Branch branch in branchesList) {
      _branches[branch.id ?? 0] = branch;
      _branchCoords[branch.id ?? 0] = await _getLatLngForBranch(branch);
    }
  }

  Future<LatLng> _getLatLngForBranch(Branch branch) async {
    double lat = 0;
    double lng = 0;
    String branchAddress = (branch.address ?? "");
    if (branchAddress.contains("lat") && branchAddress.contains("lng")) {
      lat =
          double.parse(branchAddress.substring(4, branchAddress.indexOf("-")));
      lng = double.parse(
          branchAddress.substring(branchAddress.indexOf("lng:") + 4));
    } else {
      try {
        String url =
            "https://nominatim.openstreetmap.org/search?q=${branchAddress.replaceAll(" ", "+")}&format=json";
        HttpClientRequest request = await HttpClient().getUrl(Uri.parse(url));
        HttpClientResponse response = await request.close();
        Stream<String> ss = response.transform(const Utf8Decoder());
        List<String> contentList = await ss.asBroadcastStream().toList();
        if (contentList.isNotEmpty) {
          String scontent = contentList[0];
          List<dynamic> content = json.decode(scontent);
          if (content.isNotEmpty) {
            //TODO: what if more than one address found
            Map<String, dynamic> address = content[0] as Map<String, dynamic>;
            if (address.containsKey("lat") && address.containsKey("lon")) {
              lat = double.parse(address["lat"]);
              lng = double.parse(address["lon"]);
            }
          }
        }
      } catch (ex, stackTrace) {
        NgbtLogger.warning("Coordinates for address ${branch.address}",
            loggerName: loggerName, error: ex, stackTrace: stackTrace);
      }
    }
    return LatLng(lat, lng);
  }

  void _deviceChanged(data) async {
    _setDeviceList(await session!.api.device.list());
    notifyListeners();
  }

  void _statusConnected(Session s, User u) async {
    status = ServerSessionStatus.connected;
    NgbtLogger.info("${status.value}: ${s.sessionID} user:${u.name}",
        loggerName: loggerName);
    session = s;
    user = u;
    await _setBranches(await s.api.branch.list());
    await _setDeviceList(await s.api.device.list());
    notifyListeners();
  }

  void _statusConnecting(String ip, int port) {
    status = ServerSessionStatus.connecting;
    NgbtLogger.info("${status.value} $ip:$port", loggerName: loggerName);
    session = null;
    user = null;
    this.ip = ip;
    this.port = port;
    uri = ng1_uri(host: ip, port: port);
    deviceList = [];
    notifyListeners();
  }

  void _statusDisconnecting() {
    status = ServerSessionStatus.disconnecting;
    NgbtLogger.info("${status.value} ${session?.sessionID}",
        loggerName: loggerName);
    notifyListeners();
  }

  void _statusNotConnected([String error = ""]) {
    if (session != null && session!.isOpen) {
      try {
        session!.close();
      } on Exception catch (ex, stackTrace) {
        NgbtLogger.error("Error closssing session",
            loggerName: loggerName, error: ex, stackTrace: stackTrace);
      }
    }

    status = ServerSessionStatus.notConnected;
    NgbtLogger.info("${status.value} ${session?.sessionID} $error",
        loggerName: loggerName);
    _connectionError = error;
    session = null;
    user = null;
    ip = null;
    port = null;
    uri = null;
    deviceList = [];
    notifyListeners();
  }

  String serverSessionStatusMessage(BuildContext context) {
    return status == ServerSessionStatus.notConnected
        ? AppLocalizations.of(context)!.serverSessionStatus_notConnected
        : AppLocalizations.of(context)!
            .byId("serverSessionStatus_" + status.value, [ip, port]);
  }
}

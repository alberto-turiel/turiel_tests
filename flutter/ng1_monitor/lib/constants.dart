// Flutter imports:
import 'package:flutter/material.dart';

/// Layout constants and strings used in the application
class AppConst {
  // This class is not meant to be instantiated or extended; this constructor
  // prevents instantiation and extension.
  AppConst._();

  // This info is mainly for the Live public web builds of the examples.
  // When I build a new public version I just make sure to update this info
  // before building them.
  static const String appName = 'NG1 Monitor';
  static const String version = '0.0.3';
  static const String packageVersion = '$appName package $version';
  static const String copyright = '© 2021';
  static const String author = 'NG Branch Technology GmbH';
  //static const String license = 'BSD 3-Clause License';
  static const String icon = 'assets/images/ng1_monitor.png';
  static Image image = Image.asset(icon);

  // The max dp width used for layout content on the screen in the available
  // body area. Wider content gets growing side padding, kind of like on most
  // web pages when they are used on super wide screen. Just a design used for
  // this demo app, that works pretty well in this use case.
  //static const double maxBodyWidth = 1000;
}

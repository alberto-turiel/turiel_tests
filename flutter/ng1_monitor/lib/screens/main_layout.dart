// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:ngbt_components/ngbt_locale.dart';
import 'package:provider/provider.dart';

// Project imports:
import '/components/server_connect.dart';
import '/components/server_disconnect.dart';
import '/l10n/app_localizations.g.dart';
import '/server_session.dart';
import 'dashboard_screen.dart';
import 'device_details_screen.dart';
import 'device_list_screen.dart';
import 'settings_screen.dart';

final Map<String, _Menu> _menuOptions = {
  "menu_dashboard": _Menu(const DashboardScreen(), Icons.dashboard),
  "menu_device_list": _Menu(const DeviceListScreen(), Icons.list),
  "menu_device_details": _Menu(const DeviceDetailsScreen(), Icons.table_view),
  "menu_settings": _Menu(const SettingsScreen(), Icons.settings),
};

class _Menu {
  _Menu(this.widget, this.icon);

  final Widget widget;
  final IconData icon;
}

class MainLayout extends StatefulWidget {
  const MainLayout({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<MainLayout> createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> {
  /// selected page
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const AppTitle(),
        actions: [
          const ServerDisconnect(),
          NgbtLocaleSelector(
            languageName: AppLocalizations.of(context)!.language_name,
            supportedLocales: AppLocalizations.supportedLocales,
            languageNameCallback: (BuildContext context) {
              return AppLocalizations.of(context)!.language_name;
            },
          )
        ],
        backgroundColor: Theme.of(context).colorScheme.primary,
      ),
      floatingActionButton: const ServerConnect(),
      body: IndexedStack(
        index: _selectedIndex,
        children:
            _menuOptions.values.map((e) => Center(child: e.widget)).toList(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _menuOptions.entries
            .map((e) => BottomNavigationBarItem(
                  icon: Icon(e.value.icon),
                  label: AppLocalizations.of(context)!.byId(e.key),
                ))
            .toList(),
        currentIndex: _selectedIndex,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        elevation: 16,
        onTap: (int index) => setState(() {
          _selectedIndex = index;
        }),
      ),
    );
  }
}

class AppTitle extends StatelessWidget {
  const AppTitle({Key? key}) : super(key: key);

  String getText(BuildContext context, ServerSession session) {
    String retValue = AppLocalizations.of(context)!.home_title;

    retValue += " · " + session.serverSessionStatusMessage(context);
    retValue += getUserName(session.user);

    return retValue;
  }

  String getUserName(User? user) {
    String retValue = "";
    if (user != null) {
      if (user.displayName != null) {
        retValue = " (${user.displayName})";
      } else if (user.name != null) {
        retValue = " (${user.name})";
      }
    }
    return retValue;
  }

  void _showSnackBar(BuildContext context, ServerSession session) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Row(
        children: [
          Icon(Icons.link_off, color: Theme.of(context).colorScheme.onPrimary),
          NgbtUiUtils.horizontalSeparator(),
          Text(AppLocalizations.of(context)!
              .byId('server_connect_error_${session.error}'))
        ],
      ),
      duration: NgbtUiUtils.snackBarDuration,
      action: SnackBarAction(
        label: MaterialLocalizations.of(context).closeButtonLabel,
        onPressed: () {},
      ),
    ));
    session.cleanError();
  }

  void _showSnackBarOnlyIfNeeded(BuildContext context, ServerSession session) {
    if (session.isError) {
      // to avoid error >> setState() or markNeedsBuild called during build
      Future.delayed(Duration.zero, () async {
        _showSnackBar(context, session);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ServerSession>(builder: (context, session, child) {
      _showSnackBarOnlyIfNeeded(context, session);

      return Text(getText(context, session));
    });
  }
}

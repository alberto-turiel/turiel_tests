// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:ngbt_components/ngbt_locale.dart';
import 'package:ngbt_components/ngbt_theme.dart';

// Project imports:
import '/constants.dart';
import '/l10n/app_localizations.g.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Card(
        child: Container(
            constraints: const BoxConstraints(maxWidth: 800),
            //padding: const EdgeInsets.all(NgbtUiUtils.defaultPadding),
            //decoration: BoxDecoration(
            //borderRadius: NgbtUiUtils.defaultBorderRadius,
            //color: Theme.of(context).dividerColor,
            //),
            child: ListView(
                //scrollDirection: Axis.vertical,
                //shrinkWrap: true,
                padding: const EdgeInsets.all(NgbtUiUtils.defaultPadding),
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(AppLocalizations.of(context)!.settings_language,
                            style: theme.textTheme.headline4),
                        NgbtLocaleSelector(
                          languageName:
                              AppLocalizations.of(context)!.language_name,
                          supportedLocales: AppLocalizations.supportedLocales,
                          languageNameCallback: (BuildContext context) {
                            return AppLocalizations.of(context)!.language_name;
                          },
                        ),
                      ]),
                  NgbtUiUtils.verticalSeparator(1),
                  TextButton(
                    onPressed: () => showAboutDialog(
                        context: context,
                        applicationIcon: AppConst.image,
                        applicationName: AppConst.appName,
                        applicationVersion: AppConst.version,
                        applicationLegalese:
                            "${AppConst.copyright} ${AppConst.author}.\n\nBlah blah"),
                    child: Text(
                      AppLocalizations.of(context)!.settings_about,
                    ),
                  ),
                  NgbtUiUtils.verticalSeparator(1),
                  const Divider(),
                  NgbtUiUtils.verticalSeparator(2),
                  Text(AppLocalizations.of(context)!.settings_theme,
                      style: theme.textTheme.headline4),
                  NgbtThemeSelector(byId: AppLocalizations.of(context)!.byId),
                  NgbtUiUtils.verticalSeparator(2),
                  const Divider(),
                  NgbtUiUtils.verticalSeparator(),
                  Text("Theme colors", style: theme.textTheme.headline6),
                  const ShowThemeColors(),
                  NgbtUiUtils.verticalSeparator(2),
                  const Divider(),
                  NgbtUiUtils.verticalSeparator(),
                  Text("Theme showcase", style: theme.textTheme.headline6),
                  const ThemeShowcase(),
                  //NgbtUiUtils.verticalSeparator(2),
                  //const Divider(),
                  //NgbtUiUtils.verticalSeparator(),
                  //const ShowThemeColorsExtended(),
                ])));
  }
}

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ngbt_components.dart';

// Project imports:
import '/components/branch_map.dart';
import '/components/devices_table.dart';

class DeviceListScreen extends StatefulWidget {
  const DeviceListScreen({Key? key}) : super(key: key);

  @override
  _DeviceListScreenState createState() => _DeviceListScreenState();
}

class _DeviceListScreenState extends State<DeviceListScreen> {
  List<int>? _selectedDevices;

  void _selectDevice(dynamic item) {
    setState(() {
      _selectedDevices = List<int>.empty(growable: true);
      if (item != null) {
        if (item is Device) {
          int? id = item.id;
          if (id != null) {
            _selectedDevices!.add(id);
          }
        } else {}
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
        flex: 3,
        child: NgbtCard(
          child: DevicesTable(
            selectionCallback: _selectDevice,
            columns: const [
              DevicesTableColumnId.name,
              DevicesTableColumnId.branch,
              DevicesTableColumnId.address,
              DevicesTableColumnId.status,
              DevicesTableColumnId.totals,
              DevicesTableColumnId.actions,
            ],
          ),
        ),
      ),
      Expanded(
        child: NgbtCard(
          child: BranchMap(
            visibleDevices: _selectedDevices,
          ),
        ),
        flex: 2,
      )
    ]);
  }
}

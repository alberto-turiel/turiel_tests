// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ng1/table_mix.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:provider/provider.dart';

// Project imports:
import '/components/device_status_chart.dart';
import '/components/devices_table.dart';
import '/l10n/app_localizations.g.dart';
import '/server_session.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Expanded(
          flex: 3,
          child: NgbtCard(
            child: DevicesTable(
              initialView: DevicesTableView.grid,
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: ListView(
            //scrollDirection: Axis.vertical,
            //shrinkWrap: true,
            children: const <Widget>[
              NgbtCard(child: SessionMixTable()),
              NgbtCard(child: DeviceStatusChart()),
            ],
          ),
        )
      ],
    );
  }
}

class SessionMixTable extends StatelessWidget {
  const SessionMixTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ServerSession>(
        builder: (context, session, child) => getWidget(session, context));
  }

  Widget getWidget(ServerSession session, BuildContext context) {
    var total = Mix();
    for (var device in session.deviceList) {
      total += Mix.fromDevice(device);
    }

    return TableMix(
      mix: total,
      byId: AppLocalizations.of(context)!.byId,
      title: AppLocalizations.of(context)!.mix_from_device,
      //subtitle: AppLocalizations.of(context)!.all_network,
    );
  }
}

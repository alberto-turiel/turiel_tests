// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ng1/table_cash_units.dart';
import 'package:ngbt_components/ng1/table_journal.dart';
import 'package:ngbt_components/ng1/table_mix.dart';
import 'package:ngbt_components/ngbt_components.dart';
import 'package:ngbt_log/ngbt_log.dart';
import 'package:provider/provider.dart';

// Project imports:
import '/components/device_transactions_chart.dart';
import '/l10n/app_localizations.g.dart';
import '/server_session.dart';

DateFormat dateFormat = DateFormat("dd/MM/yyyy");

class DeviceDetailsScreen extends StatefulWidget {
  const DeviceDetailsScreen({Key? key}) : super(key: key);

  @override
  _DeviceDetailsScreenState createState() => _DeviceDetailsScreenState();
}

class _DeviceDetailsScreenState extends State<DeviceDetailsScreen> {
  Device? device;
  TextEditingController dateController = TextEditingController();
  DateTime date = DateTime.now();
  List<dynamic> transactionData = [];

  @override
  Widget build(BuildContext context) {
    return Consumer<ServerSession>(
        builder: (context, session, child) => getWidget(session, context));
  }

  void loadData() async {
    List<dynamic> transactionData = await _getTransactionData(device!, date);

    setState(() {
      this.transactionData = transactionData;
    });
  }

  //
  Widget getWidget(ServerSession session, BuildContext context) {
    var deviceList = session.deviceList;

    var items = deviceList.map((dev) {
      return DropdownMenuItem(value: dev, child: Text(dev.name!));
    }).toList();

    // ensure our selected device is included in the updated device list
    if (device != null) {
      try {
        deviceList.firstWhere((dev) => dev.id == device!.id);
      } on StateError {
        // if niot clean selection
        device = null;
        transactionData.clear();
      }
    }

    return Column(
      children: [
        NgbtCard(
          child: DeviceSelectionAndInfo(
            device: device,
            //deviceList: _deviceList.value,
            items: items,
            onChanged: items.isEmpty
                ? null
                : (Device? newValue) {
                    setState(() {
                      device = newValue;
                    });
                  },
            dateController: dateController,
            date: date,
            onDatePicker: (DateTime newValue) {
              setState(() {
                date = newValue;
                dateController.text = dateFormat.format(date);
                try {
                  loadData();
                } catch (ex, stackTrace) {
                  NgbtLogger.warning("DeviceSelectionAndInfo",
                      origin: this, error: ex, stackTrace: stackTrace);
                }
              });
            },
          ),
        ),
        Expanded(
          //flex: 3,
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: ListView(
                  //scrollDirection: Axis.vertical,
                  //shrinkWrap: true,
                  children: <Widget>[
                    NgbtCard(
                      child: TableMix(
                        mix: Mix.fromDevice(device),
                        byId: AppLocalizations.of(context)!.byId,
                        title: AppLocalizations.of(context)!.mix_from_device,
                        //subtitle: device?.physicalType,
                      ),
                    ),
                    NgbtCard(
                      child: TableCashUnits(
                        cashUnits: device?.cashUnits,
                        byId: AppLocalizations.of(context)!.byId,
                        //subtitle: device?.physicalType,
                      ),
                    ),
                    NgbtCard(
                      child: DeviceTransactionsChart(
                        transactionData: transactionData,
                        title: AppLocalizations.of(context)!
                            .chars_device_transactions_title,
                        subtitle: device?.physicalType,
                      ),
                    ),
                    /*DashboardContainer(
                          child: BarChartSample2(
                            transactionData: transactionData,
                          ),
                        ),*/
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: NgbtCard(
                  child: TableJournal(
                    transactionData: transactionData,
                    byId: AppLocalizations.of(context)!.byId,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  /// get transaction data from current session
  Future<List<dynamic>> _getTransactionData(
      Device? device, DateTime date) async {
    var serverSession = Provider.of<ServerSession>(context, listen: false);

    if (device == null ||
        serverSession.status != ServerSessionStatus.connected) {
      return [];
    }

    List<dynamic> entries = await serverSession.session!.api.journal.getEntries(
      query: JournalQuery(
        date: date,
        branch: device.branchName,
        messageTypes: [
          'TRANSACTION_END',
          'DEVICE_DEPOSITED',
          'DEVICE_ROLLBACK',
          'DEVICE_WITHDRAWN',
          'BALANCE_CORRECTION'
        ],
        transactionsOnly: true,
      ),
    );

    //entries.sort((a, b) => DateTime.parse(a['timestamp']).compareTo(DateTime.parse(b['timestamp'])));
    entries = entries.reversed.toList();

    List<dynamic> retValue = <dynamic>[];
    // keep track of transactions device amounts
    Map<int, Amount?> transactions = <int, Amount>{};

    for (var entry in entries) {
      var messageType = entry['message_type'];

      var transId = entry['transaction_id'];
      // calculate transaction amount for an speciic device
      if (entry['device_name'] == device.name) {
        switch (messageType) {
          case 'DEVICE_DEPOSITED':
          case 'BALANCE_CORRECTION':
            transactions[transId] = transactions.containsKey(transId)
                ? transactions[transId]! + Amount.fromKey(entry['amount'])
                : Amount.fromKey(entry['amount']);
            break;
          case 'DEVICE_ROLLBACK':
          case 'DEVICE_WITHDRAWN':
            transactions[transId] = transactions.containsKey(transId)
                ? transactions[transId]! - Amount.fromKey(entry['amount'])
                : Amount.fromKey(entry['amount'])! * -1;
            break;
          default:
        }
      }

      // add only transaction ends
      if (messageType == 'TRANSACTION_END') {
        if (transactions.containsKey(transId)) {
          entry['device_amount'] = transactions[transId];
          retValue.insert(0, entry);
          transactions.remove(transId);
        }
      } else {
        // TODO: remove if we only want TRANSACTION_END
        retValue.insert(0, entry);
      }
    }

    return retValue;
  }
}

class DeviceSelectionAndInfo extends StatelessWidget {
  const DeviceSelectionAndInfo({
    Key? key,
    required this.items,
    this.device,
    this.onChanged,
    required this.dateController,
    required this.date,
    required this.onDatePicker,
  }) : super(key: key);

  final List<DropdownMenuItem<Device>> items;
  final Device? device;
  final void Function(Device? newValue)? onChanged;
  final TextEditingController dateController;
  final DateTime date;
  final void Function(DateTime newValue) onDatePicker;

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      Text(
        "Device:",
        style: Theme.of(context).textTheme.headline5,
      ),
      NgbtUiUtils.horizontalSeparator(),
      DropdownButton<Device>(
        //autofocus: true,
        hint: const Text("Select device..."),
        disabledHint: const Text("No devices"),
        value: device /*?? (items.isEmpty ? null : items[0].value)*/,
        items: items,
        onChanged: onChanged,
      ),
    ];

    if (device != null) {
      //children.add(Chip(label:Text(device.category != null ? device.category!.value : "?")));
      //children.add(NgbtUiUtils.horizontalSeparator());
      //children.add(Chip(label: Text(device!.physicalType ?? "?")));

      children.add(NgbtUiUtils.horizontalSeparator(2));
      //children.add(Text("Select date", style: Theme.of(context).textTheme.headline5,));
      //children.add(NgbtUiUtils.horizontalSeparator());
      children.add(
        SizedBox(
          width: 150,
          child: TextFormField(
            validator: (value) {
              return (value == "") ? "Please select a date" : null;
            },
            autovalidateMode: AutovalidateMode.always,
            textAlign: TextAlign.center,
            controller: dateController,
            onTap: () async {
              var date = await showDatePicker(
                  context: context,
                  initialDate: this.date,
                  firstDate: DateTime(2020),
                  lastDate: DateTime(2029));
              if (date != null) {
                onDatePicker(date);
              }
            },
          ),
        ),
      );
    }

    return Container(
        margin: const EdgeInsets.all(NgbtUiUtils.smallPadding),
        //color: Theme.of(context).dialogBackgroundColor,
        child: Row(
          children: children,
          mainAxisAlignment: MainAxisAlignment.center,
        ));
  }
}

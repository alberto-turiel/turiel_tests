// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:ng1_21/ng1_utils.dart';
import 'package:ngbt_components/ngbt_table_builder.dart';
import 'package:ngbt_components/ngbt_utils.dart';

import 'nga_cash_history.dart';

//
enum _Column {
  dateTime,
  device,
  total,
  diff,
  transaction,
  info,
}

class CashHistoryTable extends ConsumerStatefulWidget {
  const CashHistoryTable({
    super.key,
    required this.name,
    required this.cash,
    required this.transactions,
  });

  final String name;
  final Map<DateTime, CashUnitsState> cash;
  final Map<DateTime, TransactionState> transactions;

  @override
  ConsumerState<CashHistoryTable> createState() => _ScreenState();
}

class _ScreenState extends ConsumerState<CashHistoryTable> {
  //
  @override
  Widget build(BuildContext context) {
    var data = {...widget.cash.keys, ...widget.transactions.keys}.toList();

    data.sort((a, b) => a.compareTo(b));

    return NgbtTableBuilder<DateTime>(
      data: data,

      columns: _Column.values,
      columnBuilder: _columnBuilder,

      render: _render,
      //byId: AppLocalizations.of(context)!.byId,
      //headerTitle: "title",
      // TODO: calculate minWith based on the visible columns ??
      //minWidth: 3000,
      //rowExtent: 50,

      rowDecoration: _rowDecoration,

      headerTitle: widget.name,
      headerFilter: "Filter device",
      onFilterEntry: _onFilterEntry,
    );
  }

  bool _onFilterEntry(
    List<String> filter,
    DateTime data,
    Map<dynamic, NgbtTableBuilderColumn> visibleColumns,
    BuildContext context,
  ) {
    var cashUnitState = widget.cash[data];

    if (cashUnitState != null) {
      return NgbtDartUtils.filterMatched(filter, cashUnitState.device);
    }

    var transactionState = widget.transactions[data];

    if (transactionState != null) {
      return NgbtDartUtils.filterMatched(filter, transactionState.device);
    }

    return true;
  }

  Decoration? _rowDecoration(
    DateTime data,
    int index,
    BoxDecoration decoration,
    BuildContext context,
  ) {
    return widget.cash[data] != null
        ? decoration.copyWith(color: Colors.green[100])
        : widget.transactions[data]?.type == TransactionType.cashIn
            ? decoration.copyWith(color: Colors.blue[100])
            : decoration.copyWith(color: Colors.red[100]);
  }

  List<NgbtTableBuilderColumn> _columnBuilder(dynamic id, bool visible) {
    switch (id) {
      case _Column.dateTime:
        return [
          _createColumn(
            id,
            visible: visible,
            width: 240,
            aligment: NgbtTableBuilderColumnAlign.center,
          )
        ];

      case _Column.device:
        return [
          _createColumn(
            id,
            visible: visible,
            width: 300,
            aligment: NgbtTableBuilderColumnAlign.center,
          )
        ];

      case _Column.total:
      case _Column.diff:
      case _Column.transaction:
        return [
          _createColumn(
            id,
            visible: visible,
            width: 150,
            aligment: NgbtTableBuilderColumnAlign.right,
          )
        ];

      case _Column.info:
        return [
          _createColumn(
            id,
            visible: visible,
          )
        ];
    }

    return [];
  }

  //
  NgbtTableBuilderColumn _createColumn(
    dynamic id, {
    Object? group,
    String? Function(BuildContext context)? getGroupLabel,
    NgbtTableBuilderColumnAlign? aligment,
    int flex = 1,
    double? width,
    //double? maxWidth,
    //double? minWidth,
    bool visible = true,
    NgbtTableBuilderColumnSort sort = NgbtTableBuilderColumnSort.disabled,
  }) {
    return NgbtTableBuilderColumn(
      id: id,
      group: group,
      getGroupLabel: getGroupLabel,
      flex: flex,
      width: width,
      //minWidth: width ?? minWidth ?? 200,
      //maxWidth: width ?? maxWidth ?? 200,
      aligment: aligment,
      visible: visible,
      sort: sort,
      getLabel: (context) => id is Enum ? id.name : '???',
    );
  }

  ///
  Widget _render(
    DateTime data,
    int index,
    NgbtTableBuilderColumn column,
    BuildContext context,
  ) {
    switch (column.id) {
      case _Column.dateTime:
        return NgbtTableBuilderCellDateTime(
          column,
          data,
          format: DateFormat('yyyy-MM-dd HH:mm:ss.SSS'),
        );

      case _Column.device:
        return NgbtTableBuilderCellText(
          column,
          widget.cash[data]?.device ?? widget.transactions[data]?.device,
        );

      case _Column.total:
        var mix = widget.cash[data]?.mix;
        return NgbtTableBuilderCellAmount(
          column,
          mix?.amount,
          tooltipMix: mix,
        );

      case _Column.diff:
        var amount = widget.cash[data]?.difference;
        return NgbtTableBuilderCellAmount(
          column,
          amount,
          style: TextStyle(
              color: amount != null && amount.value >= 0
                  ? Colors.blue[600]
                  : Colors.red[600]),
        );

      case _Column.info:
        var mix = widget.cash[data]?.mix;
        if (mix != null) {
          return NgbtTableBuilderCellText(
            column,
            'getStatus: $mix',
            tooltip: widget.cash[data]?.lines.join('\n'),
          );
        }
        mix = widget.transactions[data]?.mix;
        if (mix != null) {
          var items = widget.transactions[data]?.line.split(' ');
          return NgbtTableBuilderCellText(
            column,
            '${widget.transactions[data]?.type.name}: ${mix.toString()} >>> ${items?.last}',
            tooltip: widget.transactions[data]?.line,
          );
        }
        return NgbtUiUtils.emptyWidget;

      case _Column.transaction:
        return NgbtTableBuilderCellAmount(
          column,
          widget.transactions[data]?.mix.amount,
          tooltipMix: widget.transactions[data]?.mix,
          style: TextStyle(
            color: widget.transactions[data]?.type == TransactionType.cashIn
                ? Colors.blue[600]
                : Colors.red[600],
          ),
        );
    }

    // return const Text('TODO');
    return NgbtUiUtils.emptyWidget;
  }
}

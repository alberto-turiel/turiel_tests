import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ng1_21/ng1_currencies.dart';

import 'nga_cash_history_table.dart';

// >> CashDeviceStatusResponse
const String preffixGetStatus = 'IDeviceController.END.GetStatus';
// >> CashSetResponse
const String preffixDispense = 'IDeviceController.END.Dispense';
// >> CashSetResponse
const String preffixCashIn = 'IDeviceController.END.CashInEnd';
// >> CashListResponse
const String preffixReset = 'IDeviceController.END.Reset';
// >> CashListResponse
const String preffixEmpty = 'IDeviceController.END.Empty';

//
final DateFormat _dateFormat = DateFormat('yyyy-MM-dd HH:mm:ss.SSS');

//
class NgaCashHistory extends StatefulWidget {
  const NgaCashHistory({super.key, required this.stream, required this.name});

  final Stream<List<int>> stream;
  final String name;

  @override
  State<NgaCashHistory> createState() => _NgaCashHistoryState();
}

class _NgaCashHistoryState extends State<NgaCashHistory> {
  var _historyCash = <DateTime, CashUnitsState>{};
  var _historyTransactions = <DateTime, TransactionState>{};

  @override
  void initState() {
    _readLines();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant NgaCashHistory oldWidget) {
    if (oldWidget.stream.hashCode != widget.stream.hashCode) {
      _readLines();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    debugPrint('>>>>> ${_historyCash.length}');
    return Expanded(
      child: CashHistoryTable(
        name: widget.name,
        cash: _historyCash,
        transactions: _historyTransactions,
      ),
    );
  }

  void _process(List<String> lines) {
    var history = <DateTime, CashUnitsState>{};
    var readingUnits = false;
    var mixMap = <String, int>{};
    var mixLines = <String>[];
    DateTime when = DateTime(2021, 1, 1);
    String? device;
    var lastMix = <String, Mix>{};

    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];
      if (!readingUnits && line.startsWith(preffixGetStatus)) {
        readingUnits = true;
        mixMap = {};
        when = _parseDate(lines[i - 1]);
        device = _parseDevice(line);
        mixLines = [lines[i++], lines[i++], lines[i++], lines[i]];
      } else if (readingUnits) {
        if (line.isEmpty) {
          readingUnits = false;
          var mix = Mix.fromMap(mixMap);
          var newState = CashUnitsState(device, mix, mixLines);

          // avoid duplicated
          if (lastMix[device!] == null || lastMix[device] != mix) {
            if (lastMix[device!] != null) {
              newState.difference = mix.amount! - lastMix[device]!.amount;
            }
            lastMix[device!] = mix;
            history[when] = newState;
            debugPrint('** ${history.length} ** $when - ${mix.amount} $mix');
          }
        } else {
          try {
            var items = line.split('|');
            if (items.length == 9) {
              mixLines.add(line);
              var denom = '${items[4].trim()}00_${items[1].trim()}';
              if (mixMap.containsKey(denom)) {
                mixMap[denom] = (mixMap[denom] ?? 0) + int.parse(items[2]);
              } else {
                mixMap[denom] = int.parse(items[2]);
              }
            }
          } catch (ex) {
            //debugPrint('$line >>> $ex');
          }
        }
      }
    }

    setState(() {
      _historyCash = history;
      _historyTransactions = _processTransactions(lines);
    });
  }

  Map<DateTime, TransactionState> _processTransactions(List<String> lines) {
    var history = <DateTime, TransactionState>{};
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];

      if (line.startsWith(preffixDispense)) {
        //
        var when = _parseDate(lines[i - 1]);
        var device = _parseDevice(line);
        var mix = _parseMix(lines[++i]);
        history[when] =
            TransactionState(device, TransactionType.dispense, mix, line);
        debugPrint('-- ${history.length} ** $when - ${mix.amount} $line');
      } else if (line.startsWith(preffixCashIn)) {
        //
        var when = _parseDate(lines[i - 1]);
        var device = _parseDevice(line);
        var mix = _parseMix(lines[++i]);
        history[when] =
            TransactionState(device, TransactionType.cashIn, mix, line);
        debugPrint('++ ${history.length} ** $when - ${mix.amount} $line');
      }
    }
    return history;
  }

  Mix _parseMix(String text) {
    var mixMap = <String, int>{};
    //CashSet< Curr:USD Bundles: 50_Bill_1 10_Bill_5 6_Bill_10 2_Bill_20 >

    var items = text.split(' ');
    for (var item in items) {
      if (item.contains('_')) {
        var subItems = item.split('_');
        if (subItems.length == 3) {
          var count = int.parse(subItems[0]);
          var value = int.parse(subItems[2]);
          mixMap['${value}00_USD'] = count;
        }
      }
    }

    return Mix.fromMap(mixMap);
  }

  DateTime _parseDate(String line) {
    try {
      return _dateFormat.parse(line.substring(1, 24).replaceAll(',', '.'));
    } catch (ex) {
      return DateTime(2021, 1, 1);
    }
  }

  String? _parseDevice(String line) {
    var start = line.indexOf('(');
    var end = line.indexOf(')');

    if (start >= 0 && end >= 0 && start < end) {
      var tmp = line.substring(start + 1, end);
      var items = tmp.split(',');
      return items[0];
    }
    return null;
  }

  ///
  void _readLines() async {
    //Stream<List<int>> makeStream(List<int> list) async* {yield list;}

    var lines = <String>[];

    //makeStream(widget.bytes.toList())
    widget.stream
        .transform(utf8.decoder)
        .transform(const LineSplitter())
        .listen((String line) {
      lines.add(line);
    }, onDone: () {
      _process(lines);
    }, onError: (e) {
      _process([]);
      debugPrint(e.toString());
    });
  }
}

class CashUnitsState {
  String? device;
  final Mix mix;
  final List<String> lines;
  Amount? difference;

  CashUnitsState(this.device, this.mix, this.lines);
}

class TransactionState {
  String? device;
  final TransactionType type;
  final Mix mix;
  final String line;

  TransactionState(this.device, this.type, this.mix, this.line);
}

enum TransactionType {
  dispense,
  cashIn,
  // TODO:
  reset,
  empty,
}

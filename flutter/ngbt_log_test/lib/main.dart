import 'package:flutter/material.dart';
import 'package:ngbt_components/ng1/form_field_number.dart';
import 'package:ngbt_log/ngbt_log.dart';

void main() {
  NgbtLogger.setLevel(NgbtLogger.DEBUG);
  runApp(const MyApp());
  NgbtLogger.start(console: false);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //title: 'NGBT Logger Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'NGBT Logger Test'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime? _started;
  Duration _total = const Duration(milliseconds: 0);
  var _iterations = 10;
  var _current = 0;

  Future<dynamic> runTest(int number) {
    var elements = List<int>.generate(number, (i) => i + 1);
    return Future.forEach<int>(elements, (i) {
      NgbtLogger.debug(
          '$i / $number ${DateTime.now().difference(_started!).inMilliseconds}ns');
      setState(() {
        _current = i;
      });
    });
  }

  void _startTest() async {
    setState(() {
      _started = DateTime.now();
      //
      NgbtLogger.info("Test $_iterations ------ ${NgbtLogger.getLevel()}");
      //
      runTest(_iterations).then((n) {
        _total = DateTime.now().difference(_started!);
        NgbtLogger.info(
            "Test END $_iterations => ${_total.inMilliseconds}ms ------");

        setState(() {
          _started = null;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            NumberFormField(
              initialValue: 10,
              onChanged: (value) {
                if (value != null) {
                  setState(() {
                    _iterations = value.toInt();
                  });
                }
              },
            ),
            const Text(''),
            Text(
              'Ellapsed time for $_iterations is ${_total.inMilliseconds}ms [$_current]',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _started != null ? null : _startTest,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

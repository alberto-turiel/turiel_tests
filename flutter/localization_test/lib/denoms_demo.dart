// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:ng1_null_safe/ng1_null_safe.dart' show Mix;
import 'package:ngbt_components/ng1/list_denominations.dart';

class DenomListDemo extends StatelessWidget {
  const DenomListDemo({Key? key, this.denoms}) : super(key: key);

  final Map<String, int>? denoms;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: DenominationsList(mix: Mix.fromMap(denoms)),
    );
  }
}

const Map<String, int> usdDenoms = {
  '1_USD': 1,
  '5_USD': 2,
  '10_USD': 3,
  '25_USD': 4,
  '50_USD': 5,
  '100_USD:COIN': 6,
  '100_USD:NOTE': 1,
  '200_USD': 2,
  '500_USD': 3,
  '1000_USD': 4,
  '2000_USD': 5,
  '5000_USD': 6,
  '10000_USD': 7
};

const Map<String, int> cadDenoms = {
  '1_CAD': 1,
  '5_CAD': 2,
  '10_CAD': 3,
  '25_CAD': 4,
  '50_CAD': 5,
  '100_CAD': 6,
  '200_CAD': 7,
  '500_CAD': 3,
  '1000_CAD': 1,
  '2000_CAD': 2,
  '5000_CAD': 3,
  '10000_CAD': 4
};

const Map<String, int> eurDenoms = {
  '1_EUR': 1,
  '2_EUR': 2,
  '5_EUR': 3,
  '10_EUR': 4,
  '25_EUR': 5,
  '50_EUR': 6,
  '100_EUR': 7,
  '200_EUR': 8,
  '500_EUR': 1,
  '1000_EUR': 2,
  '2000_EUR': 3,
  '5000_EUR': 4,
  '10000_EUR': 5,
  '20000_EUR': 6,
  '50000_EUR': 7
};

const Map<String, int> gbpDenoms = {
  '1_GBP': 1,
  '2_GBP': 2,
  '5_GBP': 3,
  '10_GBP': 4,
  '20_GBP': 5,
  '25_GBP': 6,
  '50_GBP': 7,
  '100_GBP': 7,
  '200_GBP': 8,
  '500_GBP': 1,
  '1000_GBP': 2,
  '2000_GBP': 3,
  '5000_GBP': 4,
};

// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:localization_test/currencies_demo.dart';
import 'package:localization_test/denoms_demo.dart';
import 'package:localization_test/editors_demo.dart';

// Package imports:

class MyDemos extends StatelessWidget {
  MyDemos({Key? key}) : super(key: key);

  final currencies = ['USD', 'CAD', 'GBP', 'EUR'];
  final amounts = ['500', '12000', '123456'];
  final denoms = ['1000', '10'];

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        CurrenciesDemo(
          currencies: currencies,
          amounts: amounts,
          denoms: denoms,
        ),
        const Spacer(flex: 1),
        const ExtraDemos(children: [
          DenomListDemo(denoms: usdDenoms),
          DenomListDemo(denoms: cadDenoms),
          DenomListDemo(denoms: gbpDenoms),
          DenomListDemo(denoms: eurDenoms),
          AmountEditorsDemo(),
          NumbersEditorsDemo(),
        ]),
        const Spacer(flex: 1),
      ],
    );
  }
}

class ExtraDemos extends StatelessWidget {
  const ExtraDemos({Key? key, required this.children}) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: children,
    );
  }
}

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:ng1_null_safe/ng1_null_safe.dart' show Amount, Denomination;
import 'package:ngbt_components/ng1/label_amount.dart';
import 'package:ngbt_components/ng1/label_denomination.dart';

class CurrenciesDemo extends StatelessWidget {
  const CurrenciesDemo({
    Key? key,
    required this.currencies,
    required this.amounts,
    required this.denoms,
  }) : super(key: key);

  final List<String> currencies;
  final List<String> amounts;
  final List<String> denoms;

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: <DataColumn>[
        getColumn('Currency'),
        getColumn('Amount'),
        getColumn('Amount'),
        getColumn('Amount'),
        getColumn('Denomination'),
        getColumn('Denomination'),
      ],
      rows: getRows(),
    );
  }

  DataColumn getColumn(String lbl) => DataColumn(
        label: Text(
          lbl,
          textAlign: TextAlign.right,
          style: const TextStyle(fontStyle: FontStyle.italic),
        ),
      );

  List<DataRow> getRows() {
    List<DataRow> retValue = [];

    for (var curr in currencies) {
      retValue.add(DataRow(cells: getCells(curr)));
    }

    return retValue;
  }

  List<DataCell> getCells(String curr) {
    List<DataCell> retValue = [];
    retValue.add(getCell(Text(curr)));

    for (var item in amounts) {
      retValue
          .add(getCell(AmountLabel(amount: Amount.fromKey('${item}_$curr'))));
    }

    for (var item in denoms) {
      retValue.add(getCell(
          DenominationLabel(denom: Denomination.fromKey('${item}_$curr'))));
    }

    return retValue;
  }

  DataCell getCell(Widget wgt) => DataCell(Align(
        child: wgt,
        alignment: Alignment.centerRight,
      ));
}

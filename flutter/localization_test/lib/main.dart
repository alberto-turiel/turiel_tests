// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:ng1_null_safe/ng1_null_safe.dart';
import 'package:ngbt_components/ngbt_locale.dart';

// Project imports:
import 'package:localization_test/demos.dart';

// Project imports:

void main() {
  ngbtSetDefaultLocale("en");
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends ConsumerWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    /// NG1: Load currencies
    rootBundle.loadString('assets/currencies.json').then((String data) {
      rootBundle
          .loadString('assets/currencies_extra.json')
          .then((String extraData) {
        Currencies.init([data, extraData]);
      });
    });

    //Currencies.initFromFuture(rootBundle.loadString('assets/currencies.json'));

    Locale currentLocale = ref.watch(ngbtLocaleProvider);

    return Builder(
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        //locale: const Locale('es', 'ES'),
        locale: currentLocale,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.home_title),
        actions: [
          NgbtLocaleSelector(
            languageName: AppLocalizations.of(context)!.language_name,
            supportedLocales: AppLocalizations.supportedLocales,
            languageNameCallback: (BuildContext context) {
              return AppLocalizations.of(context)!.language_name;
            },
          )
        ],
      ),
      body: Center(
        child: MyDemos(),
      ),
    );
  }
}

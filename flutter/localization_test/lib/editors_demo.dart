// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:ng1_null_safe/ng1_null_safe.dart' show Amount;
import 'package:ngbt_components/ng1/form_field_amount.dart';
import 'package:ngbt_components/ng1/form_field_number.dart';

class AmountEditorsDemo extends StatelessWidget {
  const AmountEditorsDemo({Key? key}) : super(key: key);

  AmountFormField _amountEditor(
          AmountEditingController controller, String lbl) =>
      AmountFormField(
        controller: controller,
        decoration: InputDecoration(
          //icon: const Icon(Icons.attach_money),
          hintText: lbl,
          labelText: controller.supportedCurrencies.join(' · '),
        ),
        onSaved: (value) => print('onSaved: $value'),
        onChanged: (value) => print('onChanged: $value'),
        validator: (value) => value == null ? "error" : null,
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minWidth: 100, maxWidth: 180),
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _amountEditor(
            AmountEditingController(amount: Amount.fromKey('12050_EUR')),
            "Amount 1",
          ),
          _amountEditor(
            AmountEditingController(currencies: ['EUR']),
            "Amount 2",
          ),
          _amountEditor(
            AmountEditingController(currencies: ['USD', 'CAD', 'EUR']),
            "Amount 3",
          ),
          _amountEditor(
            AmountEditingController(currencies: ['CAD', 'USD']),
            "Amount 4",
          ),
        ],
      ),
    );
  }
}

class NumbersEditorsDemo extends StatelessWidget {
  const NumbersEditorsDemo({Key? key}) : super(key: key);

  NumberFormField _numberEditor(
          NumberEditingController controller, String lbl) =>
      NumberFormField(
        controller: controller,
        decoration: InputDecoration(
          //icon: const Icon(Icons.format_list_numbered),
          hintText: lbl,
          labelText: '${controller.format} (${controller.number.runtimeType})',
        ),
        onSaved: (value) => print('onSaved: $value'),
        onChanged: (value) => print('onChanged: $value'),
        validator: (value) => value == null ? "error" : null,
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minWidth: 100, maxWidth: 150),
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _numberEditor(
            NumberEditingController(),
            "Number 1",
          ),
          _numberEditor(
            NumberEditingController(number: 0.0),
            "Number 2",
          ),
          _numberEditor(NumberEditingController(number: 0.0, format: "#.000"),
              "Number 3"),
        ],
      ),
    );
  }
}

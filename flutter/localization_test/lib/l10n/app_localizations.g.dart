// GENERATED CODE - DO NOT MODIFY BY HAND

// part of 'app_localizations.dart';

// **************************************************************************
// L10nGenerator
// **************************************************************************

/// 2021-10-06 @ 16:25 - Processing /localization_test/lib/l10n/app_localizations.dart

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

extension AppLocalizationsEntensions on AppLocalizations {
  /// get translation of the specific [id] in current languague
  /// If [positionalArguments] is null, it's considered an empty list.
  String byId(String id, [List<dynamic>? positionalArguments]) {
    var translations = AppLocalizationsEntensions._get(this);
    var f = translations[id];
    String retValue = '$id???';
    try {
      if (f != null) {
        retValue = Function.apply(f, positionalArguments);
      }
    } on Exception catch (ex) {
      // ignore: avoid_print
      print(ex.toString());
    }
    return retValue;
  }

  ///
  static final Map<String, Map<String, Function>> _cached = {};

  static Map<String, Function> _get(AppLocalizations al) {
    if (_cached[al.localeName] == null) {
      _cached[al.localeName] = _load(al);
    }
    return _cached[al.localeName]!;
  }

  static Map<String, Function> _load(AppLocalizations al) {
    Map<String, Function> map = {
      'localeName': () => al.localeName,
      'language_name': () => al.language_name,
      'increment': () => al.increment,
      'home_title': () => al.home_title,
      'message': (int times) => al.message(times),
    };
    return map;
  }
}

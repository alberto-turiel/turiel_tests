import os

def load_file(file_name):
    contents = []
    with open(file_name, "r") as f:
        contents = f.readlines()
    return contents

def write_file(file_name, contents):
    with open(file_name, "w") as f:
        contents = "".join(contents)
        f.write(contents)

localization_file = "app_localizations.dart"
localization_g_file = "app_localizations.g.dart"
src = ".dart_tool/flutter_gen/gen_l10n/" 
dst = "lib/l10n/"

# run flutter localization generator
os.system('flutter gen-l10n')

# load localization base file
contents = load_file(src + localization_file)

# insert 'part' after dart imports
part = "\npart 'app_localizations.g.dart';\n"
pos = 0
for i in range(len(contents)):
    if contents[i].startswith('import '):
        pos = i
contents.insert(pos+1, part)

# write modified localization file
write_file(dst + localization_file, contents)

# run flutter code generator
os.system('flutter pub run build_runner build')

# delete copied file
os.remove(dst + localization_file)

# replace ... part of 'app_localizations.dart';
# with    ... import 'package:flutter_gen/gen_l10n/app_localizations.dart';
contents = load_file(dst + localization_g_file)

for i in range(len(contents)):
    if contents[i].startswith("part of '"):
        contents[i] = "// " + contents[i]
    if contents[i].startswith("//import"):
        contents[i] = contents[i].replace("//import", "import")

write_file(dst + localization_g_file, contents)
